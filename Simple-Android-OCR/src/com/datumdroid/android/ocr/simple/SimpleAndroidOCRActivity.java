package com.datumdroid.android.ocr.simple;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.googlecode.tesseract.android.TessBaseAPI;

public class SimpleAndroidOCRActivity extends Activity implements OnCheckedChangeListener, OnClickListener{
	
	private static final int PHOTO_CAMERA = 0;
	private static final int PHOTO_GALLERY = 1;
	private static final int PHOTO_RESULT = 2;
	
	public static final String PACKAGE_NAME = "com.datumdroid.android.ocr.simple";
	public static final String DATA_PATH = Environment
			.getExternalStorageDirectory().toString() + "/SimpleAndroidOCR/";
	
	public static final String sd_path = "/sdcard/ocr-500.png";

	// You should have the trained data file in assets folder
	// You can get them at:
	// http://code.google.com/p/tesseract-ocr/downloads/list
	public static final String lang = "eng";

	private static final String TAG = "SimpleAndroidOCR.java";

	protected Button _buttonCamera;
	protected Button _buttonGallery;
	protected Button _buttonSdCard;
	private Button buttonGBitmap;
	
	private GridView mGridView;
	private CheckBox cb1;
	private CheckBox cb2;
	private CheckBox cb3;
	private CheckBox cb4;
	private CheckBox cb5;
	private CheckBox cb6;
	private List<Integer> checksList;
	private Button goButton;
	
	private ProgressDialog mProgressDialog;
	
	// protected ImageView _image;
	protected TextView _field;
	protected String _path;
	protected boolean _taken;
	private boolean isBitSpec = true;
	private OCRTestCases ocrTestCases;

	protected static final String PHOTO_TAKEN = "photo_taken";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ocrTestCases = new OCRTestCases(this);
		
		// _image = (ImageView) findViewById(R.id.image);
		_field = (TextView) findViewById(R.id.field);
		_field.setMovementMethod(ScrollingMovementMethod.getInstance());
		_buttonCamera = (Button) findViewById(R.id.camera);
		_buttonCamera.setOnClickListener(this);
		_path = DATA_PATH + "/ocr.jpg";
		
		_buttonGallery = (Button)findViewById(R.id.gallery);
		_buttonGallery.setOnClickListener(this);
		
		_buttonSdCard = (Button)findViewById(R.id.sdPath);
		_buttonSdCard.setOnClickListener(this);
		
		buttonGBitmap = (Button)findViewById(R.id.galleryForBitmap);
		buttonGBitmap.setOnClickListener(this);
		
		mGridView = (GridView)findViewById(R.id.mgridview);
		/*SimpleAdapter sImageItems = new SimpleAdapter(this, 
				getGridViewData(), 
				R.layout.grid_view_item, 
				from, 
				to);*/
		cb1 = (CheckBox)findViewById(R.id.check1);
		cb2 = (CheckBox)findViewById(R.id.check2);
		cb3 = (CheckBox)findViewById(R.id.check3);
		cb4 = (CheckBox)findViewById(R.id.check4);
		cb5 = (CheckBox)findViewById(R.id.check5);
		cb6 = (CheckBox)findViewById(R.id.check6);
		
		cb1.setOnCheckedChangeListener(this);
		cb2.setOnCheckedChangeListener(this);
		cb3.setOnCheckedChangeListener(this);
		cb4.setOnCheckedChangeListener(this);
		cb5.setOnCheckedChangeListener(this);
		cb6.setOnCheckedChangeListener(this);
		
		goButton = (Button)findViewById(R.id.checkGo);
		goButton.setOnClickListener(this);
	
	}
	
	private List<HashMap<String, Object>> getGridViewData(){
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
		return list;
	}

	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.camera:
				Log.v(TAG, "Starting Camera app");
				startCameraActivity();
				break;
			
			case R.id.gallery:
				isBitSpec = false;
				Log.v(TAG, "choose from gallery");
				Intent intent = new Intent(Intent.ACTION_PICK);
				intent.setType("image/*");
				startActivityForResult(intent, PHOTO_GALLERY);
				break;
			
			case R.id.sdPath:
				Log.v(TAG, "choose sdcard A.jpg");
				jpgPathOCR(sd_path);
				break;
				
			case R.id.galleryForBitmap:
				isBitSpec = true;
				Log.v(TAG, "choose special transmission for bitmap");
				Intent intent2 = new Intent(Intent.ACTION_PICK);
				intent2.setType("image/*");
				startActivityForResult(intent2, PHOTO_GALLERY);
				break;
				
			case R.id.checkGo:{
				checksList = new ArrayList<Integer>();
				if(cb1.isChecked()){
					checksList.add(1);
				}
				if(cb2.isChecked()){
					checksList.add(2);
				}
				if(cb3.isChecked()){
					checksList.add(3);
				}
				if(cb4.isChecked()){
					checksList.add(4);
				}
				if(cb5.isChecked()){
					checksList.add(5);
				}
				if(cb6.isChecked()){
					checksList.add(6);
				}
				if(mProgressDialog == null){
					mProgressDialog = ProgressDialog.show(this, "Processing", "Please wait...", true);
				}else {
					mProgressDialog.show();
				}
				int[] nums = new int[checksList.size()];
				for(int i=0; i<checksList.size(); i++){
					nums[i] = checksList.get(i);
				}
				String[] resStrings = ocrTestCases.OCRSomeImages(nums); 
				_field.setText("");
				for(String str : resStrings){
					_field.append(str + "\n");
				}
				mProgressDialog.dismiss();
				
				break;
			}
				
			default:
				break;
		}
	}

	// Simple android photo capture:
	// http://labs.makemachine.net/2010/03/simple-android-photo-capture/

	protected void startCameraActivity() {
		File file = new File(_path);
		Uri outputFileUri = Uri.fromFile(file);

		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

		startActivityForResult(intent, PHOTO_CAMERA);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.i(TAG, "resultCode: " + resultCode);

		if (resultCode == -1) {
			
			switch (requestCode) {
			case PHOTO_CAMERA:
				onPhotoTaken();	
				break;
			
			case PHOTO_GALLERY:
				if(data != null){
					Uri uri = data.getData();
					uriOCR(uri);
				}
				break;
			default:
				break;
			}
			
		} else {
			Log.v(TAG, "User cancelled");
		}
		
		
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean(SimpleAndroidOCRActivity.PHOTO_TAKEN, _taken);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Log.i(TAG, "onRestoreInstanceState()");
		if (savedInstanceState.getBoolean(SimpleAndroidOCRActivity.PHOTO_TAKEN)) {
			onPhotoTaken();
		}
	}

	protected void onPhotoTaken() {
		_taken = true;
		
		Log.d(TAG, "_path: " + _path);
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 4;

		Bitmap bitmap = BitmapFactory.decodeFile(_path, options);

		// _image.setImageBitmap( bitmap );

		Log.w(TAG, "Before baseApi");
		bitmapOCR(bitmap);
		
		// You now have the text in recognizedText var, you can do anything with it.
		// We will display a stripped out trimmed alpha-numeric version of it (if lang is eng)
		// so that garbage doesn't make it to the display.
	}
	
	private void uriOCR(Uri uri){
		if(uri != null){
			InputStream is = null;
			try{
				is = getContentResolver().openInputStream(uri);
				Bitmap bitmap = BitmapFactory.decodeStream(is);
				if(isBitSpec){
					specBitmapOCR(bitmap);
				}else{
					bitmapOCR(bitmap);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(is != null){
					try {
						is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private void jpgPathOCR(final String jpgPath){
		
		File file = new File(jpgPath);
		InputStream is;
		try {
			is = new FileInputStream(file);
			Bitmap bitmap = BitmapFactory.decodeStream(is);
//			bitmapOCR(bitmap);
			if(mProgressDialog == null){
				mProgressDialog = ProgressDialog.show(this, "Processing", "Please wait...", true);
			}else {
				mProgressDialog.show();
			}
			OCRAPI ocrapi = new OCRAPI(this, DATA_PATH, lang);
			String resText = ocrapi.recongniseFunc(bitmap);
			
			setOCRText(resText);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void bitmapOCR(Bitmap bitmap){
//		bitmap = bitmap.copy(Bitmap.Config.RGB_565, true);
		
		if(mProgressDialog == null){
			mProgressDialog = ProgressDialog.show(this, "Processing", "Please wait...", true);
		}else {
			mProgressDialog.show();
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] bs = baos.toByteArray();
		
		OCRAPI ocrapi = new OCRAPI(this, DATA_PATH, lang);
		String resText = ocrapi.recongniseBytes(bs);
		
		setOCRText(resText);
	}
	
	private void specBitmapOCR(Bitmap bitmap){
		if(mProgressDialog == null){
			mProgressDialog = ProgressDialog.show(this, "Processing", "Please wait...", true);
		}else {
			mProgressDialog.show();
		}
		OCRAPI ocrapi = new OCRAPI(this, DATA_PATH, lang);
		String resText = ocrapi.recongniseFunc(bitmap);
		setOCRText(resText);
	}
	
	private void setOCRText(String resText){
		Log.v(TAG, "OCRED TEXT: " + resText);
		
		if(resText.trim().length() == 0){
			_field.setText("result is empty");
		}else {
			_field.setText(resText);
		}
		
		mProgressDialog.dismiss();
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
	}

	// www.Gaut.am was here
	// Thanks for reading!
}
