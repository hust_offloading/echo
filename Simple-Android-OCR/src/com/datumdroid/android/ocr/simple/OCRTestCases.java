package com.datumdroid.android.ocr.simple;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import hust.wisemigrate_libs.aspects.Offloadable;

public class OCRTestCases implements Serializable{
	private final static String TAG = "OCRTestCases";
	
	private transient Context context;
	public List<Image> imageList;
	
	
	public OCRTestCases(Context context){
		this.context = context;
		
		try {
			imageList = new ArrayList<Image>();
			String[] imageNames = context.getResources().getAssets().list("testCases");
			for(int i=0; i<imageNames.length; i++){
				Bitmap bitmap = getImageFromAssetsFile("testCases/" + imageNames[i]);
				imageList.add(new Image(bitmap));
			}
//			imageList.add(new Image(getImageFromAssetsFile("testCases/1.jpg")));
				
		} catch (IOException e) {
		}
		
	}
	
	@Offloadable
	public String[] OCRSomeImages(int[] nums){
		String[] reStrings = new String[nums.length];
		for(int i=0; i<nums.length; i++){
			
			int j = nums[i]-1;
			Image image = imageList.get(j);
			OCRAPI ocrapi = new OCRAPI(context, SimpleAndroidOCRActivity.DATA_PATH, SimpleAndroidOCRActivity.lang);
			reStrings[i] = ocrapi.recongniseFunc(image.getBitmap());
		}
		
		return reStrings;
	}
	
	private Bitmap getImageFromAssetsFile(String fileName){
		Bitmap res = null;
		AssetManager am = context.getResources().getAssets();
		try{
			InputStream is = am.open(fileName);
			res = BitmapFactory.decodeStream(is);
			
			/*ByteArrayOutputStream baos = new ByteArrayOutputStream();
			res.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] jpegBytes = baos.toByteArray();
			
			Log.i(TAG, "bitmap size: " + res.getByteCount());
			Log.i(TAG, "jpeg size: " + jpegBytes.length);*/
			
			is.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return res;
	}
}
