package com.datumdroid.android.ocr.simple;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import hust.wisemigrate_libs.aspects.UseProxy;

@UseProxy
public class Image {
	private transient Bitmap bitmap;
	private byte[] imageBytes;
	
	public Image(Bitmap bitmap){
		this.bitmap = bitmap;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		imageBytes = baos.toByteArray();
	}
	
	public Bitmap getBitmap(){
		if(bitmap != null){
			return bitmap;
		}else if(imageBytes != null){
			return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
		}else {
			return null;
		}
	}
}
