package com.datumdroid.android.ocr.simple;

import hust.wisemigrate_libs.aspects.Offloadable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.googlecode.tesseract.android.TessBaseAPI;

public class OCRAPI {
	private static final String TAG = "OCRAPI";
	
	private transient Context context;
	private String dataPath; 
	private String lang;
	private Bitmap bitmap;
	private TessBaseAPI baseApi;
	
	public OCRAPI(Context context, String dataPath, String lang){
		Log.d(TAG, "dataPath: " + dataPath);
		Log.d(TAG, "lang: " + lang);
		this.context = context;
		this.dataPath = dataPath;
		this.lang = lang;
	}
	
	private void init(){
		String[] paths = new String[] { dataPath, dataPath + "tessdata/" };

		for (String path : paths) {
			File dir = new File(path);
			if (!dir.exists()) {
				if (!dir.mkdirs()) {
					Log.v(TAG, "ERROR: Creation of directory " + path + " on sdcard failed");
					return;
				} else {
					Log.v(TAG, "Created directory " + path + " on sdcard");
				}
			}

		}
		
		// lang.traineddata file with the app (in assets folder)
		// You can get them at:
		// http://code.google.com/p/tesseract-ocr/downloads/list
		// This area needs work and optimization
		if (!(new File(dataPath + "tessdata/" + lang + ".traineddata")).exists()) {
			try {

				AssetManager assetManager = context.getAssets();
				InputStream in = assetManager.open("tessdata/" + lang + ".traineddata");
				//GZIPInputStream gin = new GZIPInputStream(in);
				OutputStream out = new FileOutputStream(dataPath
						+ "tessdata/" + lang + ".traineddata");

				// Transfer bytes from in to out
				byte[] buf = new byte[1024];
				int len;
				//while ((lenf = gin.read(buff)) > 0) {
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				//gin.close();
				out.close();
				
				Log.v(TAG, "Copied " + lang + " traineddata");
			} catch (IOException e) {
				Log.e(TAG, "Was unable to copy " + lang + " traineddata " + e.toString());
			}
		}
	}
	
	public void setBitmap(Bitmap bitmap){
		this.bitmap = bitmap; 
	}
	
	@Offloadable
	public String recongniseFunc(Bitmap bitmap){
		init();
		baseApi = new TessBaseAPI();
		baseApi.setDebug(true);
		baseApi.init(dataPath, lang);
		baseApi.setImage(bitmap);
		
		Log.d(TAG, "recongniseFunc, bitmap length: " + bitmap.getByteCount() + " bytes");
		
		long startTime = System.currentTimeMillis();		
		String res = baseApi.getUTF8Text();
		long endTime = System.currentTimeMillis();
		Log.d(TAG, "ocr time: " + (endTime - startTime) + "ms");
		return res;
	}
	
	@Offloadable
	public String recongniseBytes(byte[] bs){
		Bitmap bitmap = BitmapFactory.decodeByteArray(bs, 0, bs.length);
		if(bitmap == null){
			return "";
		}
		else {
			return recongniseFunc(bitmap);
		}
	}
}
