# 一些实验说明
## 网络传输计时
一般情况下，网络传输时间应该分为三部分，client写入流，网络流传输，server接收完数据。这三部分时间中，client写入流的速度非常非常快，主要是后两部分占用时间多。由于client和server时间无法同步，因此采用相对计时的办法：
> client发送开始的flag，之后马上发送data。server收到flag时开始计时，知道readObject()返回，此段时间即为network transmission time。

## 客户端log中的时间
### client total time
client进入offloading流程到结束的时间

### client json time
唯一特殊的是代理场景下，real obj对象的json化不计在内，real obj会在它new的时候json化，并保存下来，以备使用。

### client network transmission time
其实只是三部分时间中，client写入流这部分时间。但是这确实是网络实际占用client执行流程的时间。（调用write函数的时间）**所以这部分是不适合直接当网络传输时间用的。**
> 如果要测这段时间，最好把gzip、encrypt关掉，因为使能的时候，这两部分时间也会计算到network transmission time里。

### client network wait time
这个时间是client阻塞在read()函数的时间。但是并不一完全等于remote pure exec time。两者的时序图有重叠部分，而且这个重叠部分可以提高效率，但是它是无法计算的。
> 代理场景下，也会把等待server ask for real的时间计入在内。

### client other time
totalTime - jsonTime - networkTransTime - networkWaitTime。**注意，并没有pureExecTime，和它无关。**

### server remote pure execution duration
服务器执行method.invoke()的时间，因此不包括网络等待、传输、json等时间。这段时间跟client的各部分时间没有对应关系，唯一的关系是，pureExecTime < networkWaitTime。
> 代理场景下，由于代理功能代码直接以字节码形式嵌入到client的代码中，因此pure execution time不太好计算，现在的计算方法是计算代理功能中网络等待、传输、json的时间，然后用invoke()调用的总时间减去他们。

## 服务器log中的时间
### server total time
执行一次方法迁移的总时间

### server pure execution time
同client，代理场景下的这个时间可能不太精确，不过误差很小，可以忽略吧。

### server json time
唯一特殊的同样存在于代理场景。代理场景下的相关对象序列化、反序列化也计算在这个时间内。

### server networkTransTime
如果一定要网络传输时间，那么networkTransTime + RTT是个不错的选择，但是并不精确，误差在于每次传输都**有可能**出现RTT/2的开销，这个误差大概在5个RTT的之内，非代理场景下我估计应该在2个RTT之内。(RTT: 往返总时间)
> 代理场景下的对象传输时间也已考虑进去。另外，同样gzip、encrypt的时间也被包括在这部分内。

### server networkWaitTime
server等待client数据的时间。简单的理解就是read()的阻塞时间。
>代理场景下，等待real obj的时间也已考虑进去。

### server other time：
totalTime - jsonTime - networkTransTime - networkWaitTime - **pureExecTime**。

## 其他
**总之，测时间的时候，关掉各种附加功能是最好的。**
