import logging
import socket
import sys
from time import sleep

from zeroconf import ServiceInfo, Zeroconf

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    if len(sys.argv) > 1:
        assert sys.argv[1:] == ['--debug']
        logging.getLogger('zeroconf').setLevel(logging.DEBUG)

    desc = {'path': '/~paulsm/'}

    info = ServiceInfo("_http._tcp.local.",
                       "wml's test web site._http._tcp.local.",
                       socket.inet_aton("202.114.6.145"), 80, 0, 0,
                       desc,
                       "ash-2.local.")

    zeroconf = Zeroconf()
    print "Registration of a service, press ctrl-c to exit..."
    zeroconf.register_service(info)

    try:
        while True:
            sleep(0.1)
    except KeyboardInterrupt:
        pass
    finally:
        print 'unregistering...'
        zeroconf.unregister_service(info)
        zeroconf.close()