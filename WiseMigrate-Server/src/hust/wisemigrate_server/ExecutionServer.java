package hust.wisemigrate_server;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class ExecutionServer extends Service {

	private static final String TAG = "WM-ExecutionServer";
	
	@Override
	public void onCreate() {
		Log.d(TAG, "Server created");
		super.onCreate();
		
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "Server destroyed");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Create server socket
		Log.d(TAG, "Start server socket");
		
		new Thread(new ServerThread(this.getApplicationContext())).start();
		
		return START_STICKY;
	}
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub	
		return null;
	}

}
