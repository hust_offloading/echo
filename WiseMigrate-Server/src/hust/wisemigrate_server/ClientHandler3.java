package hust.wisemigrate_server;

import hust.wisemigrate_libs.common.ApplicationContext;
import hust.wisemigrate_libs.common.Configuration;
import hust.wisemigrate_libs.wrapper.APKRegisterRequest;
import hust.wisemigrate_libs.wrapper.CacheKey;
import hust.wisemigrate_libs.wrapper.ExecuteMethodInfo;
import hust.wisemigrate_libs.wrapper.MyObjectIOStream;
import hust.wisemigrate_libs.wrapper.ObjectCRC32;
import hust.wisemigrate_libs.wrapper.RequestWrapper;
import hust.wisemigrate_libs.wrapper.ResponseWrapper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.SparseArray;

import com.google.gson.Gson;

public class ClientHandler3 implements Runnable{
	private static final String 	TAG	= "ClientHandler3";
	
	private final Socket			mClient;
	private final Context			mContext;
	private final int 			BUFFER = 8192;
	
	private String 					appName;						// the app name sent by the phone
	private int 					appVersionCode;					// app version code
	private String 					className;
	private	Object 					objToExecute;	// the object to be executed sent by the phone
	private	String 					methodName;						// the method to be executed
	private	Class<?>[] 				paraTypes;							// the types of the parameters passed to the method
	private	Object[] 				paraValues;						// the values of the parameters to be passed to the method
	private	String 					apkFilePath;					// the path where the apk is installed
	private long 					totalStartTime = 0;
	private long 					pureExecDuration = 0;
	private long 					serverTotalTime = 0;					// all time except network
	private long 					networkWaitTime = 0;				// during execution, time waiting for network
	private long 					networkTransTime = 0;               // network transmission time
	private long 					jsonTime = 0;
	
	private InputStream in = null;
	private DynamicObjectInputStream objIn = null;
	private ObjectOutputStream objOut = null;
	private Gson gson = null;
	private Configuration configuration = null;
	private MyObjectIOStream mIOStream = null; 
	
	private LinkedList<String> libNames;
	private HashMap<CacheKey, Object> objCacheMap = new HashMap<>();
	private HashMap<CacheKey, Object[]> parasCacheMap = new HashMap<>(); 
	
	public ClientHandler3(Socket clientSocket, final Context context) {
		
		this.mClient = clientSocket;
		this.mContext = context;
		
		try {
			gson = new Gson();
			in = mClient.getInputStream();
			objOut = new ObjectOutputStream(mClient.getOutputStream());
			objIn = new DynamicObjectInputStream(in);
		} catch(IOException e){
			e.printStackTrace();
			Log.d(TAG, "Connection failed.");
		}
	}
	
	/**
	 * Check if the application is already present on the machine
	 * 
	 * @param filename
	 *            filename of the apk file (used for identification)
	 * @return true if the apk is present, false otherwise
	 */
	private boolean apkPresent(String filename) {
		// TODO: more sophisticated checking for existence
		File apkFile = new File(filename);
		return apkFile.exists();
	}
	
	/**
	 * Method to retrieve an apk of an application that needs to be executed
	 * 
	 * @param objIn
	 *            Object input stream to simplify retrieval of data
	 * @return the file where the apk package is stored
	 * @throws IOException
	 *             throw up an exception thrown if socket fails
	 */
	private File receiveApk(DynamicObjectInputStream objIn, String apkFilePath)
			throws IOException {
		// Receiving the apk file
		// Get the length of the file receiving
		int apkLen = objIn.readInt();
		Log.d(TAG, "Read apk len - " + apkLen);

		// Get the apk file
		byte[] tempArray = new byte[apkLen];
		Log.d(TAG, "Read apk");
		objIn.readFully(tempArray);

		// Write it to the filesystem
		File dexFile = new File(apkFilePath);
		if(!(dexFile.getParentFile().exists())){
			//if parent folder not exist
			dexFile.getParentFile().mkdirs();
		}
		FileOutputStream fout = new FileOutputStream(dexFile);

		BufferedOutputStream bout = new BufferedOutputStream(fout, BUFFER);
		bout.write(tempArray);
		bout.close();
		
		return dexFile;
	}
	
	/**
	 * Extract native libraries for the x86 platform included in the .apk file
	 * (which is actually a zip file).
	 * 
	 * @param dexFile
	 *            the apk file
	 * @return the list of shared libraries' names
	 */

	@SuppressWarnings("unchecked")
	private LinkedList<String> loadApkFiles(String apkFilePath) {
		
		//Log StartTime
		Long startTime = System.nanoTime();
		
		ZipFile apkFile;
		//libName��for example libFiboJNI.so --> FiboJNI
		LinkedList<String> libNames = new LinkedList<String>();
		try {
			File dexFile = new File(apkFilePath);
			apkFile = new ZipFile(dexFile);
			Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>) apkFile
					.entries();
			ZipEntry entry;
			while (entries.hasMoreElements()) 
			{
				entry = entries.nextElement();
				Log.d(TAG, "entry.getName: " + entry.getName());
				// Zip entry for a lib file is in the form of
				// lib/platform/library.so
				// But only load x86 libraries on the server side
				if (entry.getName().matches("lib/x86/(.*).so")) {
					Log.d(TAG, "Matching Lib entry - " + entry.getName());
					
					// Store the library name to the list
					String[] words = entry.getName().split("/");
					String libFileName = words[words.length-1];
					String libName = libFileName.substring(3, (libFileName.length()-3));
					libNames.add(libName);
					
					File libFile = new File(dexFile.getParent() + "/" + libFileName);
					
					if (!libFile.exists()) {
						// Let the error propagate if the file cannot be created
						// - handled by IOException
						libFile.createNewFile();
					}
					writeFile(libFile, entry, apkFile);
					
				}else if(entry.getName().matches("assets/config.xml")){
					File configFile = new File(dexFile.getParent() + "/config.xml");
					if(!configFile.exists()){
						configFile.createNewFile();
					}
					writeFile(configFile, entry, apkFile);
					configuration = new Configuration(dexFile.getParent() + "/config.xml");
//					ApplicationContext.CLConfigurationMap.put(objIn.getClassloader().hashCode(), configuration);
					mIOStream = new MyObjectIOStream(objIn, objOut, configuration);
//					Log.e(TAG, "put iostream map hashcode: " + objIn.getClassloader().hashCode());
					ApplicationContext.CLMyIOStreamMap.put(objIn.getClassloader().hashCode(), mIOStream);
				}
			}
			apkFile.close();
		} catch (IOException e) {
			Log.d(TAG, "ERROR: File unzipping error " + e);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		Log.d(TAG, "Duration of unzipping libraries - "
				+ ((System.nanoTime() - startTime) / 1000000) + "ms");
		return libNames;
	}
	
	private void writeFile(File file, ZipEntry entry, ZipFile apkFile) throws FileNotFoundException, IOException{
		Log.d(TAG, "Writing file " + file.getAbsolutePath());
		
		FileOutputStream fos = new FileOutputStream(file);
		BufferedOutputStream dest = new BufferedOutputStream(fos,BUFFER);
		BufferedInputStream is = new BufferedInputStream(apkFile.getInputStream(entry));

		byte data[] = new byte[BUFFER];
		int count = 0;
		while ((count = is.read(data, 0, BUFFER)) != -1) {
			dest.write(data, 0, count);
		}
		dest.flush();
		dest.close();
		is.close();
	}
	
	/**
	 * Reads in the object to execute an operation on, name of the method to be
	 * executed and executes it
	 * 
	 * @param objIn
	 *            Dynamic object input stream for reading an arbitrary object
	 *            (class loaded from a previously obtained dex file inside an 
	 *            apk)
	 * @return result of executing the required method
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws OptionalDataException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws NoSuchFieldException
	 */
	private Object retrieveAndExecute() throws Exception{
				
//		Log.d(TAG, "thread classLoader: " + Thread.currentThread().getContextClassLoader().toString());
		Log.d(TAG, "Receive class name:" + className);
		Log.d(TAG, "Receive method name:" + methodName);
		
		// Get the method to be run by reflection
		Method runMethod;
		Class<?> objClass = objToExecute.getClass();
		try{
			// o's own method
			runMethod = objClass.getDeclaredMethod(methodName, paraTypes);	
		}catch(NoSuchMethodException e){
			//if the method belongs o.super() && need to be public 
			runMethod = objClass.getMethod(methodName, paraTypes);
		}
		// And force it to be accessible (quite often would be declared
		// private originally)
		runMethod.setAccessible(true); // Set the method to be accessible
		
		// Run the method and retrieve the result
		Object result = runMethod.invoke(objToExecute, paraValues);
		
		return result;
	}

	/**
	 * @param paraTypeStrings
	 * @param classLoader
	 * @return
	 * @throws ClassNotFoundException 
	 */
	private Class<?>[] getParaTypes(String[] paraTypeStrings, ClassLoader classLoader) throws ClassNotFoundException{
		Class<?>[] paraTypes = new Class<?>[paraTypeStrings.length];
		for(int i=0; i<paraTypeStrings.length; i++){
			if(paraTypeStrings[i].equals(byte.class.getName())){
				paraTypes[i] = byte.class;
			}else if(paraTypeStrings[i].equals(short.class.getName())){
				paraTypes[i] = short.class;
			}else if(paraTypeStrings[i].equals(int.class.getName())){
				paraTypes[i] = int.class;
			}else if(paraTypeStrings[i].equals(long.class.getName())){
				paraTypes[i] = long.class;
			}else if(paraTypeStrings[i].equals(double.class.getName())){
				paraTypes[i] = double.class;
			}else if(paraTypeStrings[i].equals(char.class.getName())){
				paraTypes[i] = char.class;
			}else if(paraTypeStrings[i].equals(boolean.class.getName())){
				paraTypes[i] = boolean.class;
			}else {
				paraTypes[i] = Class.forName(paraTypeStrings[i], true, classLoader);
			}
		}
		
		return paraTypes;
	}
	
	/**
	 * @param libraryNames
	 * @param loader
	 */
	public void loadLibraries(List<String> libraryNames, ClassLoader loader) {
		
		Runtime runtime = Runtime.getRuntime();
		Class<?> runClass = runtime.getClass();
		Class<?>[] paras = {String.class, ClassLoader.class};
		Method m;
		try {
			m = runClass.getDeclaredMethod("loadLibrary", paras);
			m.setAccessible(true);
			for (String libName: libraryNames) {
				Object[] parametersObjects = {libName, loader};
				m.invoke(runtime, parametersObjects);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.d(TAG, "get cause");
			Throwable cause = e.getCause();
			cause.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	public void unloadLibraries(){
		Log.d(TAG, "begin to unload libraries");
		System.gc();
		/*try {
			Field[] fields = loader.getClass().getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				System.out.println(fields[i].getName());
			}
			
			Field field = loader.getClass().getDeclaredField("nativeLibraries");
			field.setAccessible(true);
			Vector<Object> libs = (Vector<Object>) field.get(loader);
			Iterator it = libs.iterator();
			while(it.hasNext()){
				Object object = it.next();
				Method finalize = object.getClass().getDeclaredMethod("finalize");
				finalize.setAccessible(true);
				finalize.invoke(object);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}
	
	/**
	 * The Executer of remote code, which deals with the control protocol, flow
	 * of control, etc.
	 * @throws IOException 
	 */
	
	@Override
	public void run() {
		try {
			while (true) {
				int reqType = objIn.readByte();
				
				switch(reqType){
					case RequestWrapper.REQUEST_TYPE_PING:
					{
						Log.d(TAG, "Reply to PING");
						objOut.writeByte(ResponseWrapper.RESPONSE_TYPE_PONG);
						objOut.flush();
						break;
					}
					
					case RequestWrapper.REQUEST_TYPE_APK_REGISTER:
					{
						handleAPKRegister();
						System.gc();
						break;
					}
					
					case RequestWrapper.REQUEST_TYPE_EXECUTE:
					{
						serverTotalTime = 0;
						jsonTime = 0;
						pureExecDuration = 0;
						networkTransTime = 0;
						networkWaitTime = 0;
						ApplicationContext.timeJsonMap.put(objIn.getClassloader().hashCode(), 0L);
						ApplicationContext.timeWaitMap.put(objIn.getClassloader().hashCode(), 0L);
						ApplicationContext.timeTranMap.put(objIn.getClassloader().hashCode(), 0L);
						
						handleExecuteRequest();
						serverTotalTime = System.nanoTime() - totalStartTime;
						
						long proxyNetTransTime = ApplicationContext.timeTranMap.get(objIn.getClassloader().hashCode(), 0L);
						long proxyNetWaitTime = ApplicationContext.timeWaitMap.get(objIn.getClassloader().hashCode(), 0L);
						long proxyJsonTime = ApplicationContext.timeJsonMap.get(objIn.getClassloader().hashCode(), 0L);
						networkTransTime += proxyNetTransTime;
						networkWaitTime += proxyNetWaitTime;
						jsonTime += proxyJsonTime;
						
						pureExecDuration -= (proxyNetTransTime + proxyNetWaitTime + proxyJsonTime);
						
						Log.i(TAG, "Server, total time: " + serverTotalTime + "ns");
						Log.i(TAG, "Server, Pure execution time: " + pureExecDuration + " ns");
						Log.i(TAG, "Server, json time: " + jsonTime + " ns");  
						Log.i(TAG, "Server, network transmission time: " + networkTransTime + " ns");
						Log.i(TAG, "Server, network waiting for data time: " + networkWaitTime + " ns");
						Log.i(TAG, "Server, other time: " + (serverTotalTime - pureExecDuration - jsonTime - networkTransTime - networkWaitTime) + " ns");
						System.gc();
						break;
					}
					
					case RequestWrapper.REQUEST_TYPE_FINI:{
						System.gc();
						return;
					}
					
					default:
					{
						Log.d(TAG, "error msg type: " + reqType);
					}
				}
			}
			//End of While
		} catch(Exception e) {
			e.printStackTrace();
			Log.e(TAG, "Exception: " + e.toString());
		} catch(Error e) {
			e.printStackTrace();
			Log.e(TAG, "Error: " + e.toString());
		}
	}
	
	private void handleExecuteRequest() throws IOException{
		totalStartTime = System.nanoTime();
		try {
			String inStr = mIOStream.readString();
			networkTransTime += (System.nanoTime() - totalStartTime);
			
			ExecuteMethodInfo methodInfo = (ExecuteMethodInfo) fromJson(inStr, ExecuteMethodInfo.class);
			className = methodInfo.getClassName();
			methodName = methodInfo.getMethodName();
			paraTypes = getParaTypes(methodInfo.getParaTypes(), objIn.getClassloader());
			
			long waitStartTime = System.nanoTime();
			int paraInfoType = objIn.readByte();
			long waitEndTime = System.nanoTime();
			networkWaitTime += (waitEndTime - waitStartTime);
			
			switch(paraInfoType){
				case RequestWrapper.REQUEST_TYPE_PARA_EMPTY:{
					Log.d(TAG, "method has no paras");
					paraValues = null;
					paraTypes = null;
					
					break;
				} 
				
				case RequestWrapper.REQUEST_TYPE_PARA_INCACHE:{
					Log.d(TAG, "paras in cache, receive cache key");
					String keyJson = mIOStream.readString();
					networkTransTime += (System.nanoTime() - waitEndTime);
					CacheKey paraKey = (CacheKey) fromJson(keyJson, CacheKey.class);
					paraValues = parasCacheMap.get(paraKey);
					
					break;
				}
				
				case RequestWrapper.REQUEST_TYPE_PARAS:{
					String paraJson = mIOStream.readString();
					Log.d(TAG, "paras json String: " + paraJson);
					if(paraJson == null){
						throw new Exception("para msg type error");
					}
					networkTransTime += (System.nanoTime() - waitEndTime);
					String[] paraArr = (String[]) fromJson(paraJson, String[].class);
					paraValues = new Object[paraArr.length];
					for(int i=0; i<paraArr.length; i++){
						paraValues[i] = fromJson(paraArr[i], paraTypes[i]);
					}
					
					// put paras to cache
					if(configuration.isCacheEnable() && paraJson.length() > configuration.getCacheThreshold()){
						long crcVal = ObjectCRC32.computeCRC32(paraJson);
						parasCacheMap.put(new CacheKey(crcVal, paraJson.length()), paraValues);
					}
					
					break;
				}
				
				case RequestWrapper.REQUEST_TYPE_PARA_BITMAP:{
					byte[] bs = mIOStream.readBytes();
					if(bs == null){
						throw new Exception("bitmap bytes read length error");
					}
					paraValues = new Object[1];
					paraValues[0] = BitmapFactory.decodeByteArray(bs, 0, bs.length);
					
					if(configuration.isCacheEnable() && bs.length > configuration.getCacheThreshold()){
						long crcVal = ObjectCRC32.computeCRC32(bs);
						parasCacheMap.put(new CacheKey(crcVal, bs.length), paraValues);
					}
					
					break;
				}
				
				default:{
					Log.e(TAG, "para msg type error: " + paraInfoType);
					throw new Exception("para msg type error");
				}
			}
				
			waitStartTime = System.nanoTime();
			int objInfoType = objIn.readByte();
			waitEndTime = System.nanoTime();
			networkWaitTime += (waitEndTime - waitStartTime);
			switch (objInfoType) {
				case RequestWrapper.REQUEST_TYPE_OBJECT_INCACHE:{
					Log.d(TAG, "object already in cache, receive key");
					String keyJson = mIOStream.readString();
					networkTransTime += (System.nanoTime() - waitEndTime);
					CacheKey objKey = (CacheKey) fromJson(keyJson, CacheKey.class);
					objToExecute = objCacheMap.get(objKey);
					break;
				}
				
				case RequestWrapper.REQUEST_TYPE_OBJECT:{
					String objJson = mIOStream.readString();
					Log.d(TAG, "obj json String: " + objJson);
					networkTransTime += (System.nanoTime() - waitEndTime);
					objToExecute = fromJson(objJson, Class.forName(className, true, objIn.getClassloader()));
					
					// put object to cache
					if(configuration.isCacheEnable() && objJson.length() > configuration.getCacheThreshold()){
						long crcVal = ObjectCRC32.computeCRC32(objJson);
						CacheKey objKey = new CacheKey(crcVal, objJson.length());
						objCacheMap.put(objKey, objToExecute);
					}
					break;
				}
				
				default:{
					Log.e(TAG, "obj msg type error: " + paraInfoType);
					throw new Exception("obj msg type error");
				}
			}
			
			long startExecTime = System.nanoTime();
			Object result = retrieveAndExecute();
			pureExecDuration = System.nanoTime() - startExecTime;
			
			//return result
			if(result instanceof Bitmap){
				mIOStream.writeByte(ResponseWrapper.RESPONSE_TYPE_EXECUTE_RESULT_BITMAP);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				((Bitmap)result).compress(Bitmap.CompressFormat.JPEG, 100, baos);
				byte[] resBitmapBytes = baos.toByteArray();
				mIOStream.writeBytes(resBitmapBytes);
			}else{
				String resultString = toJson(result);
				mIOStream.writeByte(ResponseWrapper.RESPONSE_TYPE_EXECUTE_RESULT);
				mIOStream.writeString(resultString);
			}
			
			//return objState
			String objStateString = toJson(objToExecute);
			mIOStream.writeByte(ResponseWrapper.RESPONSE_TYPE_EXECUTE_OBJSTATE);
			mIOStream.writeString(objStateString);
			
			mIOStream.writeLong(pureExecDuration);
		} catch (Exception e){
			mIOStream.writeByte(ResponseWrapper.RESPONSE_TYPE_EXECUTE_EXCEPTION);
			e.printStackTrace();
		}
	}
	
	private String toJson(Object obj){
		long startJson = System.nanoTime();
		String res = gson.toJson(obj);
		jsonTime += (System.nanoTime() - startJson);
		
		return res;
	}
	
	private Object fromJson(String objJson, Class<?> clazz){
		long startJson = System.nanoTime();
		Object obj = gson.fromJson(objJson, clazz);
		jsonTime += (System.nanoTime() - startJson);
		return obj;
	}
	
	public void handleAPKRegister() throws IOException{
		Log.d(TAG, "APK Register");
		String regInfoJson = null;
		try {
			regInfoJson = (String)objIn.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		APKRegisterRequest apkRegister = (APKRegisterRequest)gson.fromJson(regInfoJson, APKRegisterRequest.class);
		appName = apkRegister.getAppName();
		appVersionCode = apkRegister.getVersionCode();
		
		/* folder /appName_versionCode/files* */
		String folderPath = mContext.getFilesDir().getAbsolutePath() + "/" + appName + "_" + appVersionCode; 
		apkFilePath = folderPath + "/" + appName + ".apk";
		Log.d(TAG, "apk file path: " + apkFilePath);
		
		//set file path
		objIn.setApkPath(apkFilePath);
		objIn.setDexOutFolderPath(folderPath);
		objIn.setLibFolderPath(folderPath);
		
		if(apkPresent(apkFilePath)) {
			Log.d(TAG, "APK present");
			objOut.writeByte(ResponseWrapper.RESPONSE_TYPE_APK_PRESENT);
			objOut.flush();
		} else {
			Log.d(TAG, "request APK");
			objOut.writeByte(ResponseWrapper.RESPONSE_TYPE_APK_REQUEST);
			objOut.flush();
			receiveApk(objIn, apkFilePath);
		}
		
		libNames = loadApkFiles(apkFilePath);
	
		// Log.d(TAG, "lib path: " + System.getProperty("java.library.path"));
		// load libs here to avoid error of "already opened by another class loader"
		// If libraries is not empty, add application libraries to the classloader.
		if (!libNames.isEmpty()) {
			loadLibraries(libNames, objIn.getClassloader());	
		}
	}
}
