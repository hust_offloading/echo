package hust.wisemigrate_server;

import hust.wisemigrate_libs.common.ApplicationContext;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

import android.util.Log;
import dalvik.system.DexClassLoader;

/**
 * Custom object input stream to also deal with dynamically loaded classes. The
 * classes can be retrieved from Android Dex files, provided in Apk (android
 * application) files.
 * 
 * @author Andrius
 * 
 */
public class DynamicObjectInputStream extends ObjectInputStream {
	private static final String TAG = "DynamicObjectInputStream";

	private String apkPath;
	private String dexOutFolderPath;
	private String libFolderPath;
//	private ClassLoader mCurrent = Thread.currentThread().getContextClassLoader();
	private ClassLoader mCurrent = ClassLoader.getSystemClassLoader();
	private DexClassLoader mCurrentDexLoader = null;

	public DynamicObjectInputStream(InputStream in) throws IOException {
		super(in);
	}
	
	/**
	 * Override the method resolving a class to also look into the constructed
	 * DexClassLoader
	 */
	@Override
	protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException,
			ClassNotFoundException {
		
		DexClassLoader dexClassLoader = getClassloader();
		return dexClassLoader.loadClass(desc.getName());
		
/*		try {
			try {
				return mCurrent.loadClass(desc.getName());
			} catch (ClassNotFoundException e) {
				return mCurrentDexLoader.loadClass(desc.getName());
			}
		} catch (ClassNotFoundException e) {
			return super.resolveClass(desc);
		} catch (NullPointerException e) { // Thrown when currentDexLoader is
			// not yet set up
			return super.resolveClass(desc);
		}*/

	}

	public void setApkPath(String apkPath) {
		this.apkPath = apkPath;
	}

	public void setDexOutFolderPath(String dexOutFolderPath) {
		this.dexOutFolderPath = dexOutFolderPath;
	}

	public void setLibFolderPath(String libFolderPath) {
		this.libFolderPath = libFolderPath;
	}

	/**
	 * Add a Dex file to the Class Loader for dynamic class loading for clients
	 * 
	 * @param apkFile
	 *            the apk package
	 */
	public void addDex(final File apkFile) {
		if (mCurrentDexLoader == null)
		{
			mCurrentDexLoader = new DexClassLoader(apkFile.getAbsolutePath(),
					apkFile.getParentFile().getAbsolutePath(), null, mCurrent);
		}
		else
		{	mCurrentDexLoader = new DexClassLoader(apkFile.getAbsolutePath(),
					apkFile.getParentFile().getAbsolutePath(), null,
					mCurrentDexLoader);
		}
		
		Log.d(TAG, "addDex apkFilePath: " + apkFile.getAbsolutePath());
		Log.d(TAG, "apkDex apkParentFilePath: " + apkFile.getParentFile().getAbsolutePath());
	}
	
	/**
	 * Add a Dex file to the Class Loader with native libraries.
	 * @param apkFile
	 * @param libraryPath
	 */
	/*public void addDex(final File apkFile, String libraryPath) {
		Log.d(TAG, "addDex, libraryPath: " + libraryPath);
		if (mCurrentDexLoader == null)
		{
			mCurrentDexLoader = new DexClassLoader(apkFile.getAbsolutePath(),
					apkFile.getParentFile().getAbsolutePath(), libraryPath, mCurrent);
		}
		else
		{
			mCurrentDexLoader = new DexClassLoader(apkFile.getAbsolutePath(),
					apkFile.getParentFile().getAbsolutePath(), libraryPath,
					mCurrentDexLoader);
		}
		
		Log.d(TAG, "addDex apkFilePath: " + apkFile.getAbsolutePath());
		Log.d(TAG, "apkDex apkParentFilePath: " + apkFile.getParentFile().getAbsolutePath());
	}*/
	
	/**
	 * Get the instance of Dexclassloader
	 */
	public DexClassLoader getClassloader()
	{
		if (mCurrentDexLoader == null) {
			mCurrentDexLoader = new DexClassLoader(apkPath, 
					   dexOutFolderPath, 
					   libFolderPath, 
					   mCurrent);

//			Log.d(TAG, "thread CL: " + Thread.currentThread().getContextClassLoader());
//			Log.d(TAG, "thread CL hashCode: " + Thread.currentThread().getContextClassLoader().hashCode());
//			Log.d(TAG, "mCurrentDexLoader hashCode: " + mCurrentDexLoader.hashCode());
//			Log.d(TAG, "obj inputstream: " + this.hashCode());
		}
		return mCurrentDexLoader;
	}
}
