package hust.wisemigrate_server;

import hust.wisemigrate_libs.common.ApplicationContext;
import hust.wisemigrate_libs.common.Configuration;
import hust.wisemigrate_libs.ControlConstants;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import android.content.Context;
import android.util.Log;

public class ServerThread implements Runnable{
	private static final String TAG = "ServerThread";
	ServerSocket serverSocket;
	
	private Context context;
	
	public ServerThread(Context context) {
		this.context = context;
		ApplicationContext.getInstance().setContext(context);
		ApplicationContext.getInstance().setPackageName(context.getPackageName());
		ApplicationContext.getInstance().setResources(context.getResources());
	}

	
	@Override
	public void run() {
		Log.d(TAG, "Server Thread running");	
		try {
			serverSocket = new ServerSocket(Configuration.getInstance().getServerPort());
			while(true) {
				Socket clientSocket;
				clientSocket = serverSocket.accept();
				Log.d(TAG, "Client connected");
				//new ClientHandler(clientSocket, context, config);
				new Thread(new ClientHandler3(clientSocket, context)).start();
			} 
		} catch(IOException e) {
			Log.e(TAG, "IOException: " + e.getMessage());
		} catch(Exception e) {
			Log.v(TAG, "Exception: " + e.getMessage());
		} finally {
			try {
				serverSocket.close();
				Log.d(TAG, "Socket is now closed");
			} catch (Exception e) {
				Log.d(TAG, "Socket was never opened");
			}
		}
	}
}
