package hust.wisemigrate_client;

import hust.wisemigrate_client.benchmarks.Fibo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class FiboThread extends Thread{
	private final static String TAG = "FiboThread";
	private Handler mHandler = null;
	private int n;
	private Fibo fibo;
	public FiboThread(Handler mHandler, Fibo fibo, int n) {
		this.mHandler = mHandler;
		this.n = n;
		this.fibo = fibo;
	}
	
	@Override
	public void run() {
		int res = fibo.localfirstStepFib(n);
		Log.d(TAG, "res: " + res);
		Message message = new Message();
		message.what = MainActivity.EXEC_RESULT;
		message.obj = res;
		mHandler.sendMessage(message);
	}
}
