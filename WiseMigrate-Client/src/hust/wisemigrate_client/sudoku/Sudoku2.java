package hust.wisemigrate_client.sudoku;

import hust.wisemigrate_libs.aspects.Offloadable;
import android.util.Log;

public class Sudoku2 {
	private static final long serialVersionUID = -3962977915411306215L;
	private static final String TAG = "Sudoku2";
	
	private int[][] matrix;
	private Sudoku2Input sudoku2Input = new Sudoku2Input();

	public Sudoku2() {
		Log.d(TAG, "new Sudoku2");
	}

	@Offloadable
	public boolean localhasSolution() {
		matrix = sudoku2Input.getInput();
		
//		Log.d(TAG, "sudoku2.class CL: " + this.getClass().getClassLoader().toString());
//		Log.d(TAG, "Thread CL: " + Thread.currentThread().getContextClassLoader().toString());
		
		boolean res = solve(0, 0, matrix);
		
		SolverUtil.display(matrix);
		return res;
	}

	public boolean solve(int i, int j, int[][] cells) {
		if (i == 9) {
			i = 0;
			if (++j == 9)
				return true;
		}
		if (cells[i][j] != 0)
			return solve(i + 1, j, cells);
		for (int val = 1; val <= 9; ++val) {
			if (legal(i, j, val, cells)) {
				cells[i][j] = val;
				if (solve(i + 1, j, cells))
					return true;
			}
		}
		cells[i][j] = 0;
		return false;
	}

	private boolean legal(int i, int j, int val, int[][] cells) {
		for (int k = 0; k < 9; ++k)
			if (val == cells[k][j])
				return false;
		for (int k = 0; k < 9; ++k)
			if (val == cells[i][k])
				return false;
		int boxRowOffset = (i / 3) * 3;
		int boxColOffset = (j / 3) * 3;
		for (int k = 0; k < 3; ++k)
			for (int m = 0; m < 3; ++m)
				if (val == cells[boxRowOffset + k][boxColOffset + m])
					return false;
		return true;
	}
}
