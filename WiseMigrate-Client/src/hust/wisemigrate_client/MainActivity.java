package hust.wisemigrate_client;

import hust.wisemigrate_client.benchmarks.Fibo;
import hust.wisemigrate_client.queens.NQueens2;
import hust.wisemigrate_client.sudoku.Sudoku2;
import hust.wisemigrate_libs.ControlConstants;
import hust.wisemigrate_libs.OffloadingController3;
import hust.wisemigrate_libs.ServerHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	
	public final static int EXEC_RESULT = 1;
	
	private static final String TAG = "Client-MainActivity";
	private final static int TASKTYPE_INVALID = 0;
	private final static int TASKTYPE_FIBO = 1;
	private final static int TASKTYPE_SUDOKU = 2;
	private final static int TASKTYPE_QUEEN = 3;
	private final static int TASKTYPE_FIBO3 = 4;
	
	private Handler 		    	mhandler;
	private int                    taskType;
	
	
	private Button sudokuButton;
	private Button fiboButton;
	private Button fibo3Button;
	private Button queensButton;
	private Spinner queensSpinner;
	private ProgressDialog progressDialog;
	
	private Fibo fibo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mhandler = new MyHandler();
		
	/*	//TODO, using a thread to handle OffloadingController socket connect.
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()  
        .detectDiskReads()  
        .detectDiskWrites()  
        .detectNetwork()   // or .detectAll() for all detectable problems  
        .penaltyLog()  
        .build());  
		
		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()  
        .detectLeakedSqlLiteObjects()  
        .detectLeakedClosableObjects()  
        .penaltyLog()  
        .penaltyDeath()  
        .build());  */
		
		createNotOffloadedFile();
		getComponents();
		setListeners();
		System.loadLibrary("FiboJNI");
	}
	private void getComponents(){
		sudokuButton = (Button)findViewById(R.id.buttonSudoKu);
		fiboButton = (Button)findViewById(R.id.buttonFibo);
		fibo3Button = (Button)findViewById(R.id.buttonFiboController3);
		queensButton = (Button)findViewById(R.id.queensSolver);
		queensSpinner = (Spinner)findViewById(R.id.spinnerNrQueens);
		progressDialog = new ProgressDialog(this);
	}
	
	private void setListeners(){
		sudokuButton.setOnClickListener(this);
		fiboButton.setOnClickListener(this);
		fibo3Button.setOnClickListener(this);
		queensButton.setOnClickListener(this);
	}
	
	/**
	 * Create an empty file on the phone in order to let the method know
	 * where is being executed (on the phone or on the clone).
	 */
	private void createNotOffloadedFile(){
		try {
			File f = new File(ControlConstants.FILE_NOT_OFFLOADED);
			f.createNewFile();
		} catch (FileNotFoundException e) {
			Log.e(TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
	}
	
/*	public void onClickQueenSolver(View v) {
		try {
			int nQueens = Integer.parseInt((String)queensSpinner.getSelectedItem());
			
			Log.i(TAG, "Number Queens: " + nQueens);
			NQueens2 solution = new NQueens2(nQueens);
			
			
			Class<?>[] parameterTypes = {int.class};
			Object[] parameterValue = {nQueens};
			Method m = solution.getClass().getDeclaredMethod("localSolveNQueens", parameterTypes);
			int result = (Integer)offloadingController3.execute(m, parameterValue, solution);
			
			Log.i(TAG, "NQueens problem solved, solutions: " + result);
			Toast.makeText(MainActivity.this, "NQueens problem solved, solutions: " + result, Toast.LENGTH_SHORT).show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}*/
	
	@Override
	public void onClick(View v) {
		progressDialog.setMessage("waiting for result...");
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.show();
		
		switch (v.getId()) {
			case R.id.buttonFibo:{
				progressDialog.dismiss();
				taskType = TASKTYPE_FIBO;
				Toast.makeText(MainActivity.this, "controller not set, use fibo3", Toast.LENGTH_LONG).show();
				break;
			}
			case R.id.buttonFiboController3:{
				taskType = TASKTYPE_FIBO3;
				fibo = new Fibo();
				new FiboThread(mhandler, fibo, 10).start();
				break;
			}
			case R.id.buttonSudoKu:{
				taskType = TASKTYPE_SUDOKU;
				new SudokuThread(mhandler).start();
				break;
			}
			case R.id.queensSolver:{
				taskType = TASKTYPE_QUEEN;
				int nQueens = Integer.parseInt((String)queensSpinner.getSelectedItem());
				
				Log.i(TAG, "Number Queens: " + nQueens);
				
				new QueenThread(mhandler, nQueens).start();
				
				progressDialog.dismiss();
				
				break;
			}

			default:
			{
				Log.d(TAG, "task type error");
				break;
			}
		}
	}
	
	private void handleResult(Object resultObj)
	{
		progressDialog.dismiss();
		
		switch (taskType) {
			case TASKTYPE_FIBO:{
				int result = (Integer)resultObj;
				Log.i(TAG, "Fibo problem solved, solutions: " + result);
				Toast.makeText(MainActivity.this, "Fibo result: " + result, Toast.LENGTH_LONG).show();
				break;
			}
			case TASKTYPE_FIBO3:{
				int result = (Integer)resultObj;
				Log.i(TAG, "Fibo problem solved, solutions: " + result);
				Toast.makeText(MainActivity.this, "Fibo-controller3 result: " + result + " , str: " + fibo.str, Toast.LENGTH_LONG).show();
				break;
			}
			case TASKTYPE_QUEEN:{
				int result = (Integer)resultObj;
				Log.i(TAG, "NQueens problem solved, solutions: " + result);
				Toast.makeText(MainActivity.this, "NQueens problem solved, solutions: " + result, Toast.LENGTH_SHORT).show();
				break;
			}
			case TASKTYPE_SUDOKU:{
				boolean result = (Boolean)resultObj;
				Log.d(TAG, "Sudoku result: " + String.valueOf(result));
				Toast.makeText(MainActivity.this, "Sudoku result: " + String.valueOf(result), Toast.LENGTH_LONG).show();
				break;
			}
			default:{
				Log.d(TAG, "task type error");
				break;
			}
		}
	}
	
	@SuppressLint("HandlerLeak") 
	private class MyHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) 
			{
				case MainActivity.EXEC_RESULT:
				{
					handleResult(msg.obj);
					break;					
				}
				default:
				{
					Log.d(TAG, "msgType error");
					break;
				}
			}
		}
	}
}
