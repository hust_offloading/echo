package hust.wisemigrate_client;

import hust.wisemigrate_client.sudoku.Sudoku2;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class SudokuThread extends Thread{
	private final static String TAG = "SudokuThread";
	Handler mHandler = null;
	public SudokuThread(Handler mHandler) {
		this.mHandler = mHandler;
	}
	
	@Override
	public void run() {
		boolean res = new Sudoku2().localhasSolution();
		
//		Log.d(TAG, "res: " + res + ", str: " + Sudoku2.helloString);
		Message message = new Message();
		message.what = MainActivity.EXEC_RESULT;
		message.obj = res;
		mHandler.sendMessage(message);
	}
}
