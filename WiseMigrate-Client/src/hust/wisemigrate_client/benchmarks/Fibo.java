// $Id: fibo.java,v 1.2 2000/12/24 19:10:50 doug Exp $

package hust.wisemigrate_client.benchmarks;

import java.io.Serializable;

import hust.wisemigrate_libs.aspects.Offloadable;


public class Fibo implements Serializable{
	
	private static final long serialVersionUID = -3962977915411306215L;
	public String str = "hello";
	
	public Fibo() {
	}


	@Offloadable
	public int localfirstStepFib(int n) {
		str = str + "world";
		if (n < 2)
			return (1);
		return (fib(n - 2) + fib(n - 1));
	}

	/*public int fib(int n) {
		if (n < 2)
			return (1);
		return (fib(n - 2) + fib(n - 1));
	}*/
	
	public native int fib(int n);
}
