package hust.wisemigrate_client;

import hust.wisemigrate_client.benchmarks.Fibo;
import hust.wisemigrate_client.queens.NQueens2;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class QueenThread extends Thread{
	private final static String TAG = "FiboThread";
	private Handler mHandler = null;
	private int n;
	public QueenThread(Handler mHandler, int n) {
		this.mHandler = mHandler;
		this.n = n;
	}
	
	@Override
	public void run() {
		int res = new NQueens2(n).localSolveNQueens(n);
		Log.d(TAG, "res: " + res);
		Message message = new Message();
		message.what = MainActivity.EXEC_RESULT;
		message.obj = res;
		mHandler.sendMessage(message);
	}
}
