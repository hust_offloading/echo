#include <jni.h>
JNIEXPORT jint JNICALL Java_hust_wisemigrate_1client_benchmarks_Fibo_fib
  (JNIEnv *env, jobject obj, jint n)
  {
	if(n < 2)
		return 1;
	return (Java_hust_wisemigrate_1client_benchmarks_Fibo_fib(env, obj, n-2)
			+ Java_hust_wisemigrate_1client_benchmarks_Fibo_fib(env, obj, n-1));
  }