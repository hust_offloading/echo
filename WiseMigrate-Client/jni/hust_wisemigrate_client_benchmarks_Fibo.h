/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class hust_wisemigrate_client_benchmarks_Fibo */

#ifndef _Included_hust_wisemigrate_client_benchmarks_Fibo
#define _Included_hust_wisemigrate_client_benchmarks_Fibo
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     hust_wisemigrate_client_benchmarks_Fibo
 * Method:    fib
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_hust_wisemigrate_1client_benchmarks_Fibo_fib
  (JNIEnv *, jobject, jint);

#ifdef __cplusplus
}
#endif
#endif
