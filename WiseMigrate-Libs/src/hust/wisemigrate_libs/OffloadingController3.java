package hust.wisemigrate_libs;

import hust.wisemigrate_libs.aspects.OffloadingProxy;
import hust.wisemigrate_libs.common.Configuration;
import hust.wisemigrate_libs.profilers.DeviceProfiler;
import hust.wisemigrate_libs.profilers.LogRecord;
import hust.wisemigrate_libs.profilers.NetworkProfiler;
import hust.wisemigrate_libs.profilers.Profiler;
import hust.wisemigrate_libs.profilers.ProgramProfiler;
import hust.wisemigrate_libs.wrapper.APKRegisterRequest;
import hust.wisemigrate_libs.wrapper.CacheKey;
import hust.wisemigrate_libs.wrapper.ExecuteMethodInfo;
import hust.wisemigrate_libs.wrapper.MyObjectIOStream;
import hust.wisemigrate_libs.wrapper.ObjectCRC32;
import hust.wisemigrate_libs.wrapper.RequestWrapper;
import hust.wisemigrate_libs.wrapper.ResponseWrapper;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.DataFormatException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * The interface to the framework for the client program - controls
 * OffloadingSolver, profilers, communicates with remote server
 * @author ll
 *
 */

public class OffloadingController3 {
	private static final String TAG = "OffloadingController3";
	
	private long 				mPureExecutionDuration = 0;
	
	public static final int 	REGIME_CLIENT = 1;
	public static final int		REGIME_SERVER = 2;
	private static int 			mRegime;
	
	public LogRecord			lastLogRecord;
	public LogRecord			lastLocalRecord;
	public LogRecord			lastRemoteLogRecord;
	
	private Server 				mServer;
	private String				mAppName;
	private Context 			mContext;
	private PackageManager		mPManager;
	
	private boolean 			onLine;
	private OffloadingSolver	mSolver;
	private DeviceProfiler		mDevProfiler;
	private NetworkProfiler		mNetProfiler;
	private ProgramProfiler		mProgProfiler;
	private Profiler 			mProfiler;
	
	private Socket				mSocket;
	private MyObjectIOStream    mIOStream;
	private Configuration       configuration;
	
	private Set<CacheKey> objCacheKeySet = new HashSet<CacheKey>(); 
	private Set<CacheKey> paraCacheKeySet = new HashSet<CacheKey>();
	private Gson gson = new Gson();
	private long jsonTime = 0;
	private long networkTransTime = 0;
	private long networkWaitTime = 0;
	
	public OffloadingController3(Server server, 
			String appName, PackageManager pManager, Context context) {
		
		mRegime = REGIME_CLIENT;
		
		this.mServer	= server;
		this.mAppName	= appName;
		this.mPManager	= pManager;
		this.mContext	= context;
		
		//mDevProfiler	= new DeviceProfiler(context);
		mDevProfiler = DeviceProfiler.getInstance(context);
		//mDevProfiler.trackBatteryLevel();
		//netProfiler = new NetworkProfiler(context);
		mNetProfiler = NetworkProfiler.getInstance(context);
		mNetProfiler.registerNetworkStateTrackers();
		
		try {
			configuration = Configuration.getInstance();
			establishConnection();
		} catch (Exception e) {
			Log.d(TAG, "encrypt exception");
			e.printStackTrace();
		} 
	}
	
	private synchronized void establishConnection() {
		Log.d(TAG, "Establish Connection.");
		
		try {
			Long sTime = System.nanoTime();
			
			Log.d(TAG, "Target Server Ip: " + mServer.getIp() + 
					"; Target Server Port: " + mServer.getPort());
			
			InetAddress inetAddress = InetAddress.getByName(mServer.getIp());
			mSocket = new Socket(inetAddress, mServer.getPort());
			
			//Add no delay option, which keep the socket transmit data immediately
			mSocket.setTcpNoDelay(true);
			ObjectOutputStream objOut = new ObjectOutputStream(mSocket.getOutputStream());
			ObjectInputStream objIn = new ObjectInputStream(mSocket.getInputStream());
			mIOStream = new MyObjectIOStream(objIn, objOut, configuration);
			onLine = true;
			
			Long dur = System.nanoTime() - sTime;
			Log.d(TAG, "Socket and streams set-up time - "
					+ dur / 1000000 + "ms");
			//Find rtt
			NetworkProfiler.rttPing(mIOStream);
			
			//make it always remote
			mSolver = new OffloadingSolver(OffloadingSolver.EXECUTION_LOCATION_STATIC_REMOTE);
			//Test the decision engine
			//mSolver = new OffloadingSolver(OffloadingSolver.EXECUTION_LOCATION_DYNAMIC);
			
			//send apk register request
			mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_APK_REGISTER);
			
			Log.d(TAG, "Getting apk data, appName: " + mAppName);
			PackageInfo packInfo = mPManager.getPackageInfo(mAppName,0);  
			int versionCode = packInfo.versionCode;
			APKRegisterRequest apkRegisterRequest = new APKRegisterRequest(mAppName, versionCode);
			
			mIOStream.writeObject(toJson(apkRegisterRequest));
			
			if(ResponseWrapper.RESPONSE_TYPE_APK_REQUEST == mIOStream.readByte()){
				//send Apk
				String apkName = mPManager.getApplicationInfo(mAppName, 0).sourceDir;
				Log.d(TAG, "Apk full path- " + apkName);	//Including full path
				sendApk(apkName, mIOStream);
			}

		} catch (UnknownHostException e) {
			fallBackToLocalExecution("Connection setup to server failed: " + e.getMessage());
		} catch (IOException e) {
			fallBackToLocalExecution("Connection setup to server failed: " + e.getMessage());
		} catch (NameNotFoundException e) {
			fallBackToLocalExecution("Application not found: " + e.getMessage());
		} catch (Exception e) {
			fallBackToLocalExecution("Could not connect: " + e.toString());
			e.printStackTrace();
		}
	}
	
	private void fallBackToLocalExecution(String message) {
		Log.d(TAG, "fall back to local execution");		
		Log.d(TAG, message);

		mSolver = new OffloadingSolver(OffloadingSolver.EXECUTION_LOCATION_STATIC_LOCAL);
		
		synchronized(this) {
			onLine = false;
		}
	}
	
	/**
	 * Wrapper of the execute method with no return for the executable method
	 * 
	 * @param methodName
	 * @param o
	 * @throws NoSuchMethodException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws IllegalArgumentException 
	 * @throws InvocationTargetException 
	 * @throws IOException 
	 * @throws DataFormatException 
	 */
	public void executeVoid(Method m, Object o) 
			throws IllegalArgumentException, 
			    	SecurityException, 
			    	IllegalAccessException, 
			    	ClassNotFoundException, 
			    	NoSuchMethodException,
			    	InvocationTargetException,
			    	IOException, DataFormatException{
		//for void method，execute will return null
		execute(m, (Class<?> [])null, (Object[])null, o);
	}
	
	/**
	 * @param methodName
	 * @param pTypes
	 * @param pValues
	 * @param o
	 * @throws NoSuchMethodException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws IllegalArgumentException 
	 * @throws InvocationTargetException 
	 * @throws IOException 
	 * @throws DataFormatException 
	 */
	public void executeVoid(Method m, Class<?> []pTypes, Object[] pValues, Object o) 
			throws IllegalArgumentException, 
					SecurityException, 
					IllegalAccessException,
					ClassNotFoundException, 
					NoSuchMethodException,
					InvocationTargetException, 
					IOException, DataFormatException{
		execute(m, pTypes, pValues, o);
	}
	/**
	 * Wrapper of the execute method with no parameters for the executable
	 * method
	 * 
	 * @param methodName
	 * @param o
	 * @return
	 * @throws InvocationTargetException 
	 * @throws NoSuchMethodException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws IllegalArgumentException 
	 * @throws IOException 
	 * @throws DataFormatException 
	 * @throws Throwable
	 */
	public Object execute(Method m, Object o) 
			throws IllegalArgumentException, 
			        SecurityException, 
			        IllegalAccessException, 
			        ClassNotFoundException, 
			        NoSuchMethodException, 
			        InvocationTargetException,
			        IOException, DataFormatException {
		return execute(m, (Class<?> [])null, (Object[])null, o);
	}
	
	/**
	 * Call OffloadingSolver to decide where to execute the operation, start
	 * profilers, execute (either locally or remotely), collect profiling data
	 * and return execution results.
	 * 
	 * @param methodName
	 *            method name to be executed
	 * @param pValues
	 *            with parameter values
	 * @param o
	 *            on object
	 * @return result of execution, or an exception if it happened
	 * @throws NoSuchMethodException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException 
	 * @throws IOException 
	 * @throws DataFormatException 
	 */
	public Object execute(Method m, Class<?> []pTypes, Object[] pValues, Object o)
			throws  IllegalAccessException, 
			        NoSuchMethodException, 
			        InvocationTargetException, 
			        ClassNotFoundException, 
			        IOException, IllegalArgumentException, DataFormatException{
		
		Object result = null;
		/*try{
			// o's own method(public or private)
			m = o.getClass().getDeclaredMethod(methodName, pTypes);	
		}catch(NoSuchMethodException e){
			//if the method belongs o.super() && need to be public 
			//its NoSuchMethodException is throwed out
			m = o.getClass().getMethod(methodName, pTypes);
		}*/
		
		String classMethodName = o.getClass().toString() + m.getName();
		
		Log.d(TAG, "classMethodName: " + classMethodName);
		
		if(mSolver.executeRemotely(mContext, classMethodName)) 
		{
			Log.v(TAG, "Execute Remotely.");
			//result may be exception, handle our own exception here
			//IOException & ClassNotFoundException
			jsonTime = 0;
			networkTransTime = 0;
			networkWaitTime = 0;
			long totalStartTime = System.nanoTime();
			result = executeRemotely(m, pTypes, pValues, o);
			long totalExecTime = System.nanoTime() - totalStartTime;

			Log.i(TAG, "remote pure execute duration: " + mPureExecutionDuration + " ns");
			Log.i(TAG, "Client, Total time: " + totalExecTime + " ns");
			Log.i(TAG, "Client, Json total time: " + jsonTime + " ns");
			Log.i(TAG, "Client, network transmission time: " + networkTransTime + " ns");
			Log.i(TAG, "Client, network wait time(wait for result): " + networkWaitTime + " ns");
			Log.i(TAG, "Client, other time: " + (totalExecTime - jsonTime - networkTransTime - networkWaitTime) + " ns");
			
			// exception but not m's own, execute locally
			if(result instanceof Exception && !isOwnException(result, m)){
				System.out.println("remote exception, exec locally");
				result = executeLocally(m, pValues, o);
			}
			
			return result;
		} 
		else 
		{
			Log.v(TAG, "Execute Locally.");
			//IllegalArgumentException & IllegalAccessException & InvocationTargetException
			result = executeLocally(m, pValues, o);
			return result;
		}
	}
	
	
	/**
	 * Execute the method locally
	 * 
	 * @param m
	 * @param pValues
	 * @param o
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public Object executeLocally(Method m, Object[] pValues, Object o)
			throws IllegalArgumentException, 
					IllegalAccessException,
					InvocationTargetException {
		
		//ProgramProfiler progProfiler = new ProgramProfiler(m.getName());
		//DeviceProfiler devProfiler = new DeviceProfiler(mContext);
		mProgProfiler = ProgramProfiler.getInstance();
		mProgProfiler.setMethodName(m.getName());
		
		//Profiler profiler = new Profiler(mRegime, mContext, progProfiler, null, devProfiler);
		mProfiler = Profiler.getInstance(mContext);
		mProfiler.setRemine(mRegime);
		mProfiler.setProfilers(mProgProfiler, mNetProfiler, mDevProfiler);

		// Start tracking execution statistics for the method
		//profiler.startExecutionInfoTracking();
		mProfiler.startExecutionInfoTracking();
				
		Object result = null;
		Long startTime = System.nanoTime();
		m.setAccessible(true);
		
		Log.d(TAG, "Begin to local invoke!");

		result = m.invoke(o, pValues);

		
		mPureExecutionDuration = System.nanoTime() - startTime;
		Log.d(TAG, "LOCAL " + m.getName()
				+ ": Actual Invocation duration - " + mPureExecutionDuration / 1000000
				+ "ms");
		
		//profiler.stopAndLogExecutionInfoTracking(mPureExecutionDuration);
		mProfiler.stopAndLogExecutionInfoTracking(mPureExecutionDuration);
		//lastLogRecord = profiler.lastLogRecord;
		//lastLogRecord = mProfiler.lastLogRecord;
		
		return result;
	}
	
	/**
	 * if connection is alive
	 * 
	 * @return
	 */
	public boolean isOnLine(){
		return onLine;
	}
	
	/**
	 * Execute method remotely
	 * 
	 * @param m
	 * @param pValues
	 * @param o
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws DataFormatException 
	 */
	private Object executeRemotely(Method m, Class<?> []pTypes, Object[] pValues, Object o) 
			throws IOException, 
					ClassNotFoundException, 
					IllegalArgumentException, 
					IllegalAccessException, 
					InvocationTargetException,
					DataFormatException{
		mProgProfiler = ProgramProfiler.getInstance();
		mProgProfiler.setMethodName(m.getName());
		mProfiler = Profiler.getInstance(mContext);
		mProfiler.setRemine(mRegime);
		mProfiler.setProfilers(mProgProfiler, mNetProfiler, mDevProfiler);
		mProfiler.startExecutionInfoTracking();
		
		Object result = null;
		
		//result may be exception
		//sendAndExecute throws IOException & ClassNotFoundException
		// & IllegalArgumentException, IllegalAccessException, InvocationTargetException
		try {
			result = sendAndExecute(m, pTypes, pValues, o);
			
			mProfiler.stopAndLogExecutionInfoTracking(mPureExecutionDuration);
			//lastLogRecord = mProfiler.lastLogRecord;
			
		}catch (JsonSyntaxException e) {
			Log.d(TAG, "gson.fromJson() exception, exec locally");
			result = executeLocally(m, pValues, o);
		}
		
		return result;
	}
	
	/**
	 * Send the object, the method to be executed and parameter values to the
	 * remote server for execution.
	 * 
	 * @param m
	 *            method to be executed
	 * @param pValues
	 *            parameter values of the remoted method
	 * @param o
	 *            the remoted object
	 * @param objIn
	 *            ObjectInputStream which to read results from
	 * @param objOut
	 *            ObjectOutputStream which to write the data to
	 * @return result of the remoted method or an exception that occurs during
	 *         execution
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws DataFormatException 
	 */
	private Object sendAndExecute(Method m, Class<?> []pTypes, Object[] pValues, Object o)
					throws IOException, 
							ClassNotFoundException, 
							IllegalArgumentException, 
							IllegalAccessException, 
							InvocationTargetException,
							JsonSyntaxException, 
							DataFormatException{		
		//Send the object
		//IOException
		sendObject(o, m, pTypes, pValues);
		
		//Read the results from the server
		Object res = null;
		
		long startWait = System.nanoTime();

		
		Log.d(TAG, "Waiting for Result ...");
		//ClassNotFoundException & IOException
		while(true)
		{
			long startRx = NetworkProfiler.getProcessRxBytes();
			long startTx = NetworkProfiler.getProcessTxBytes();
			
			int msgType = mIOStream.readByte();
			long startReceiveTime = System.nanoTime();
			networkWaitTime += (startReceiveTime - startWait);
			
			switch (msgType) {
				case ResponseWrapper.RESPONSE_TYPE_EXECUTE_EXCEPTION:{
					Log.d(TAG, "remote return Exception");
					res = executeLocally(m, pValues, o);
					return res;
				}
				
				case ResponseWrapper.RESPONSE_TYPE_EXECUTE_RESULT_BITMAP:{
					long tmpTime = System.nanoTime();
					byte[] resBitmapBytes = mIOStream.readBytes();
					networkTransTime += (System.nanoTime() - startReceiveTime);
					Bitmap resBitmap = BitmapFactory.decodeByteArray(resBitmapBytes, 0, resBitmapBytes.length);
					
					afterResult(o);
					
//					Log.i(TAG, "receive result object - transmit bytes: ");
					long rxBytes = NetworkProfiler.getProcessRxBytes() - startRx;
					long txBytes = NetworkProfiler.getProcessTxBytes() - startTx;
					Log.i(TAG, "result bitmap, rxBytes: " + rxBytes + ", txBytes: " + txBytes);
					NetworkProfiler.addNewBandwidthEstimate(NetworkProfiler.getProcessRxBytes()
							- startRx + NetworkProfiler.getProcessTxBytes() - startTx,
							System.nanoTime() - tmpTime);
					
					return resBitmap;
				}
				
				case ResponseWrapper.RESPONSE_TYPE_EXECUTE_RESULT:{
					
//					long tmpTime = System.nanoTime();
					String resJson = (String)mIOStream.readString();
					networkTransTime += (System.nanoTime() - startReceiveTime);
					res = fromJson(resJson, m.getReturnType());
					
					afterResult(o);
					
//					Log.i(TAG, "receive result object - transmit bytes: ");
					/*long rxBytes = NetworkProfiler.getProcessRxBytes() - startRx;
					long txBytes = NetworkProfiler.getProcessTxBytes() - startTx;
					Log.i(TAG, "result, rxBytes: " + rxBytes + ", txBytes: " + txBytes);
					NetworkProfiler.addNewBandwidthEstimate(NetworkProfiler.getProcessRxBytes()
							- startRx + NetworkProfiler.getProcessTxBytes() - startTx,
							System.nanoTime() - tmpTime);*/
					return res;
				}
				
				case ResponseWrapper.RESPONSE_TYPE_FOR_PROXY_REAL:{
					
//					long tmpTime = System.nanoTime();
					
					Log.d(TAG, "remote ask for real");
					String proxyKeyJson = (String)mIOStream.readString();
					networkTransTime += (System.nanoTime() - startReceiveTime);
					CacheKey proxyKey = (CacheKey) fromJson(proxyKeyJson, CacheKey.class);
					String realObjStr = OffloadingProxy.objRealJsonCacheMap.get(proxyKey);
					mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_MSG_PROXY_REAL);
					Log.i(TAG, "write real obj length: " + realObjStr.length());
					mIOStream.writeString(realObjStr);
					startWait = System.nanoTime();
					
					/*long rxBytes = NetworkProfiler.getProcessRxBytes() - startRx;
					long txBytes = NetworkProfiler.getProcessTxBytes() - startTx;
					Log.i(TAG, "proxy, rxBytes: " + rxBytes + ", txBytes: " + txBytes);
					NetworkProfiler.addNewBandwidthEstimate(rxBytes + txBytes, System.nanoTime() - tmpTime);*/
					break;
				}
				
				default:{
					Log.d(TAG, "return msg type error: " + msgType);
					break;
				}
			}
		}
	}
	
	private void afterResult(Object o) throws OptionalDataException, ClassNotFoundException, IOException, DataFormatException{
		if(ResponseWrapper.RESPONSE_TYPE_EXECUTE_OBJSTATE == mIOStream.readByte()){
			Log.d(TAG, "read Object State");
			long startReceObjStateTime = System.nanoTime();
			String newObjJson = (String)mIOStream.readString();
			networkTransTime += (System.nanoTime() - startReceObjStateTime);
			
			Object newObj = fromJson(newObjJson, o.getClass());
			mergeState(o, newObj);
		}
		
		mPureExecutionDuration = mIOStream.readLong();
	}
	
	private boolean isOwnException(Object result, Method m){
		if(!(result instanceof Exception)){
			return false;
		}
		
		boolean isOwnExcep = false;
		Class<?>[] methodExceps = m.getExceptionTypes();
		for(Class<?> methodExcep : methodExceps){
			//if result exception is the method's own exception 
			if(methodExcep.getClass() == result.getClass()){
				isOwnExcep = true;
				break;
			}
		}
		
		return isOwnExcep;
	}
	
	/**
	 * merge state of object executed 
	 * @param oldObj 
	 * @param newObj
	 */
	private void mergeState(Object oldObj, Object newObj){
		//newObj need to be casted
		
		Class<?> clazz = oldObj.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for(Field field : fields){
			field.setAccessible(true);
			//what about final variable?
			//class final variable won't be changed
			boolean isFinal = Modifier.isFinal(field.getModifiers());
			if(isFinal){
				continue;
			}else {
				try {
					field.set(oldObj, field.get(newObj));
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Send APK file to the remote server
	 * 
	 * @param apkName
	 *            file name of the APK file (full path)
	 * @param objOut
	 *            ObjectOutputStream to write the file to
	 * @throws IOException
	 */
	private void sendApk(String apkName, MyObjectIOStream mIoStream)
			throws IOException {
		File apkFile = new File(apkName);
		FileInputStream fin = new FileInputStream(apkFile);
		BufferedInputStream bis = new BufferedInputStream(fin);
		
		byte[] tempArray = new byte[(int) apkFile.length()];
		bis.read(tempArray, 0, tempArray.length);
		
		// Send file length first
		Log.d(TAG, "Sending apk length - " + tempArray.length);
		
		// Send the file
		long startTime = System.nanoTime();
		Log.d(TAG, "Sending apk");
		mIoStream.writeBytes(tempArray);
		long estimatedTime = System.nanoTime() - startTime;
		
		// The 1000000000 comes from measuring time in nanoseconds
		Double estimatedBandwidth = ((double) tempArray.length / (double) estimatedTime) * 1000000000;
		NetworkProfiler.addNewBandwidthEstimate(estimatedBandwidth);
		Log.d(TAG, tempArray.length + " bytes sent in "	+ estimatedTime + " ns");
		Log.d(TAG, "Estimated bandwidth - "	+ NetworkProfiler.bandwidth + " Bps");
		
		bis.close();
	}
	
	/**
	 * Send the object (along with method and parameters) to the remote server
	 * for execution
	 * methodInfo -->> objJson -->> parasJson
	 * @param o
	 * @param m
	 * @param pValues
	 * @param objOut
	 * @throws IOException
	 */
	private void sendObject(Object o, Method m, Class<?> []paraTypes, Object[] pValues) 
							throws IOException {
		String[] paraTypeStrs = new String[paraTypes.length];
		for(int i=0; i<paraTypes.length; i++){
			paraTypeStrs[i] = paraTypes[i].getName();
		}
		
		ExecuteMethodInfo methodInfo =  new ExecuteMethodInfo(o.getClass().getName(), 
				  m.getName(), 
				  paraTypeStrs);
		String methodInfoJson = toJson(methodInfo);
		
		//REQUEST_TYPE_EXECUTE also as a start flag for receive methodInfo on server
		mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_EXECUTE);
		mIOStream.writeString(methodInfoJson);
		
		boolean isParaInCache = false;
		CacheKey paraKey = null;
		String parasJson = null;
		byte[] paraBitmapBytes = null;
		if(pValues != null && pValues.length != 0){
			//para is not null

			if(configuration.isBitmapSpecific && pValues.length == 1 && (pValues[0] instanceof Bitmap)){
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				((Bitmap)pValues[0]).compress(Bitmap.CompressFormat.JPEG, 100, baos);
				paraBitmapBytes = baos.toByteArray();
				
				if(configuration.isCacheEnable() && paraBitmapBytes.length > configuration.getCacheThreshold()){
					long crcValue = ObjectCRC32.computeCRC32(paraBitmapBytes);
					paraKey = new CacheKey(crcValue, paraBitmapBytes.length);
					if(paraCacheKeySet.contains(paraKey)){
						Log.w(TAG, "bitmap in cache");
						isParaInCache = true;
					}else{
						paraCacheKeySet.add(paraKey);
						paraKey = null;
					}
				}
			}else{
				String[] paraValueStrs = new String[pValues.length];
				for(int i=0; i<paraValueStrs.length; i++){
					paraValueStrs[i] = toJson(pValues[i]);
				}		
				parasJson = toJson(paraValueStrs);
				
				//para cache
				if(configuration.isCacheEnable() && parasJson.length() > configuration.getCacheThreshold()){
					long crcValue = ObjectCRC32.computeCRC32(parasJson);
					int len = parasJson.length();
					paraKey = new CacheKey(crcValue, len);
					
					if(paraCacheKeySet.contains(paraKey)){
						isParaInCache = true;
					}else{
						paraCacheKeySet.add(paraKey);
						paraKey = null;
					}
				}
			}
		}
		
		if(parasJson == null && paraBitmapBytes == null){
			mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_PARA_EMPTY);
		}else{
			if(isParaInCache){
				//in cache
				mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_PARA_INCACHE);
				mIOStream.writeString(toJson(paraKey));
			}else{
//				Log.d(TAG, "write paras");
/*				long startRx = NetworkProfiler.getProcessRxBytes();
				long startTx = NetworkProfiler.getProcessTxBytes();
				long startSend = System.nanoTime();*/
				
				if(paraBitmapBytes != null){
					mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_PARA_BITMAP);
					mIOStream.writeBytes(paraBitmapBytes);
					Log.i(TAG, "write para, bitmap bytes length: " + paraBitmapBytes.length);
					
				}else{
					Log.i(TAG, "write para, json length: " + parasJson.length());
					mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_PARAS);
					mIOStream.writeString(parasJson);
				}
				
/*				NetworkProfiler.addNewBandwidthEstimate(NetworkProfiler.getProcessRxBytes()
						- startRx + NetworkProfiler.getProcessTxBytes() - startTx,
						System.nanoTime() - startSend);*/
			}
		}
		
		//object send
		boolean isObjInCache = false;
		CacheKey objKey = null;
		String objJson = toJson(o);
		//obj cache
		if(configuration.isCacheEnable() && objJson.length() > configuration.getCacheThreshold()){
			long crcValue = ObjectCRC32.computeCRC32(objJson);
			int len = objJson.length();
			objKey = new CacheKey(crcValue, len);
			
			if(objCacheKeySet.contains(objKey)){
				isObjInCache = true;
			}else {
				objCacheKeySet.add(objKey);
				objKey = null;
			}
		}
		
		if(isObjInCache){
			String keyJson = toJson(objKey);
			Log.i(TAG, "write object cache key, length: " + keyJson.length());
			mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_OBJECT_INCACHE);
			mIOStream.writeString(keyJson);
		}else{
			Log.i(TAG, "write object, length: " + objJson.length());
			long startRx = NetworkProfiler.getProcessRxBytes();
			long startTx = NetworkProfiler.getProcessTxBytes();
			long startSend = System.nanoTime();
			
			mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_OBJECT);
			mIOStream.writeString(objJson);
			
			NetworkProfiler.addNewBandwidthEstimate(NetworkProfiler.getProcessRxBytes()
					- startRx + NetworkProfiler.getProcessTxBytes() - startTx,
					System.nanoTime() - startSend);
		}

		//Estimate the perceived bandwidth
	}
	
	private String toJson(Object obj){
		long startJson = System.nanoTime();
		String res = gson.toJson(obj);
		jsonTime += System.nanoTime() - startJson;
		
		return res;
	}
	
	private Object fromJson(String objJson, Class<?> clazz){
		long startJson = System.nanoTime();
		Object obj = gson.fromJson(objJson, clazz);
		jsonTime += System.nanoTime() - startJson;
		return obj;
	}
	
	protected void fini(){
		try {
			mIOStream.writeByte(RequestWrapper.REQUEST_TYPE_FINI);
			mIOStream.fini();
			mSocket.close();
			onDestroy();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//TODO, handle the onDestroy in the calling activity
	public void onDestroy() {
		//mDevProfiler.onDestroy();
		mNetProfiler.onDestroy();
	}
}



