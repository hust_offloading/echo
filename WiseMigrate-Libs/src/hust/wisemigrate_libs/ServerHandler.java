package hust.wisemigrate_libs;

import android.content.Context;
import android.util.Log;
import hust.wisemigrate_libs.common.Configuration;

public class ServerHandler {
	private final static String TAG = "ServerHandler";
	
	private static ServerHandler handler = null;
	private Server server = null;	
	private OffloadingController3 offloadingController = null;
	private Configuration configuration = null;

	private ServerHandler(Context context) {
		configuration = Configuration.getInstance();
		server = new Server(configuration.getServerIp(), configuration.getServerPort());
		Log.d(TAG, "IP: " + configuration.getServerIp() + ", PORT: " + configuration.getServerPort());
		Log.d(TAG, Configuration.getInstance().printConfigInfo());
		offloadingController = new OffloadingController3(server,
			context.getPackageName(),
			context.getPackageManager(),
			context);
	}
	
	public synchronized static void initialize(Context context) {
		if(null == handler) {
			handler = new ServerHandler(context);
		}			
	}
	
	public synchronized static void checkInitialize() {
		if(null == handler) 
			throw new RuntimeException("ServerHandler is not initialized.");
	}
	
	public synchronized static ServerHandler getServerHandler() {
		checkInitialize();
		return handler;
	}
	
	public synchronized static OffloadingController3 getController(){
		checkInitialize();
		return handler.offloadingController;
	}
	
	public synchronized static boolean isInit(){
		if(null == handler){
			return false;
		} else {
			return true;
		}
	}
	
	public void fini(){
		offloadingController.fini();
	}
 }
