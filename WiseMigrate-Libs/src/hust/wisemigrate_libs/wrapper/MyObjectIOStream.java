package hust.wisemigrate_libs.wrapper;



import hust.wisemigrate_libs.common.Configuration;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.zip.DataFormatException;
import java.util.zip.DeflaterInputStream;
import java.util.zip.GZIPInputStream;

import android.util.Log;

public class MyObjectIOStream{
	private final static String TAG = "MyObjectIOStream";
	
	public ObjectInputStream objIn;
	public ObjectOutputStream objOut;
	private AESUtil aesUtil;
	
	private Configuration configuration;
	
	public MyObjectIOStream(ObjectInputStream objInput, ObjectOutputStream objOut, Configuration config){
		configuration = config;
		try {
			this.objIn = objInput;
			this.objOut = objOut;
			configuration = config;
			aesUtil = new AESUtil(configuration.getAESKey());
		} catch (NoSuchAlgorithmException e) {
			Log.d(TAG, "AESUtil init exception: NoSuchAlgorithm");
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			Log.d(TAG, "AESUtil init exception: NoSuchProvider");
			e.printStackTrace();
		}
	}
	
	public void writeString(String reqString) throws IOException{
//		Log.d(TAG, "write request String length: " + reqString.length());
//		long startWriteOp = System.nanoTime();
		byte[] reqBytes = null;
		
		//gzip
		if(configuration.isGZIPEnable() == true && reqString.length()>configuration.getGzipThreshold()){
			reqBytes = GZIPUtil.compressStr(reqString.getBytes());
			Log.d(TAG, "gziped, length: " + reqBytes.length);
		}
		
		//encrypt
		if(configuration.isEncrypted() == true && aesUtil != null){
			if(reqBytes != null){
				reqBytes = aesUtil.encrypt(reqBytes);
			}else{
				reqBytes = aesUtil.encrypt(reqString.getBytes());
			}
			Log.d(TAG, "encryped, length: " + reqString.length());
		}
		
		if(reqBytes != null){
			objOut.writeByte(RequestWrapper.REQUEST_TYPE_MSG_BYTES);
			objOut.writeInt(reqBytes.length);
			objOut.flush();
			objOut.write(reqBytes);
		}else {
			objOut.writeByte(RequestWrapper.REQUEST_TYPE_MSG_STR);
			objOut.writeObject(reqString);
		}
		
		objOut.flush();
//		Log.w(TAG, "write Option time: " + (System.nanoTime() - startWriteOp) + " ns");
	}
	
	public void writeLong(long data) throws IOException{
		objOut.writeLong(data);
		objOut.flush();
	}
	
	public void writeInt(int data) throws IOException{
//		Log.i(TAG, "write Int: " + data);
		objOut.writeInt(data);
		objOut.flush();
	}
	
	public void writeByte(byte data) throws IOException{
		objOut.writeByte(data);
		objOut.flush();
	}
	
	public void writeObject(Object obj) throws IOException{
		objOut.writeObject(obj);
		objOut.flush();
	}
	
	public String readString() throws OptionalDataException, ClassNotFoundException, IOException, DataFormatException{
		byte msgType = objIn.readByte();
		byte[] reqBytes = null;
		
		if(msgType == RequestWrapper.REQUEST_TYPE_MSG_STR){
			return (String)objIn.readObject();
		}
		else if(msgType == RequestWrapper.REQUEST_TYPE_MSG_BYTES){
			reqBytes = readBytes();
			if(reqBytes == null){
				Log.e(TAG, "read bytes msg error: " + msgType);
				return null;
			}
			
			//encrypt			
			if(configuration.isEncrypted()==true && aesUtil!=null){
				reqBytes = aesUtil.decrypt(reqBytes);
			}
			
			//Gzip
			if(configuration.isGZIPEnable() && reqBytes.length > 1){
				return GZIPUtil.unCompressStr(reqBytes);
				/*int mark = (reqBytes[0] & 0xff) | ((reqBytes[1] & 0xff) << 8 );
				if(mark == DeflaterInputStream.){
					return GZIPUtil.unCompressStr(reqBytes);
				}*/
			}
			
		}else {
			Log.e(TAG, "read String msg type error: " + msgType);
			return null;
		}
		
		return new String(reqBytes);		
	}
	
	public byte[] readBytes() throws IOException{
		int byteLen = objIn.readInt();
		byte[] reqBytes = new byte[byteLen];
		int readTotalLen = 0;
		
		while(readTotalLen < reqBytes.length){
			int readLen = objIn.read(reqBytes, readTotalLen, byteLen);
			readTotalLen += readLen;
			byteLen -= readLen;
			
			if(readLen == -1){
				break;
			}
		}
		
		if(readTotalLen != reqBytes.length){
			Log.e(TAG, "bytes read do not equal with bytes sent, readLen: " + readTotalLen);
			return null;
		}
		
		return reqBytes;
	}
	
	public void writeBytes(byte[] buf) throws IOException{
		objOut.writeInt(buf.length);
		objOut.write(buf);
		objOut.flush();
	}
	
	public long readLong() throws IOException{
		return objIn.readLong();
	}
	
	public int read(byte[] buf) throws IOException{
		return objIn.read(buf);
	}
	
	public byte readByte() throws IOException{
		return objIn.readByte();
	}
	
	public void fini() throws IOException{
		objIn.close();
		objOut.close();
	}
}
