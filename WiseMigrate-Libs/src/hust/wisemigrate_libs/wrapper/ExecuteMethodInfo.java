package hust.wisemigrate_libs.wrapper;

import java.io.Serializable;

public class ExecuteMethodInfo implements Serializable{
	
	private static final long serialVersionUID = 2L;
	
	private String className;
	private String methodName;
	private String[] paraTypes;

	public ExecuteMethodInfo(String className, 
							 String methodName, 
							 String[] paraTypes){
		this.className = className;
		this.methodName = methodName;
		this.paraTypes = paraTypes;
	}

	public String getClassName(){
		return className;
	}

	public String getMethodName() {
		return methodName;
	}

	public String[] getParaTypes() {
		return paraTypes;
	}
}
