package hust.wisemigrate_libs.wrapper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;

import android.util.Base64;
import android.util.Log;
import hust.wisemigrate_libs.common.Configuration;

public class GZIPUtil {
	private static final String TAG = "GZIPUtil";
	
	/*
	 * return String is base64 encoded, 
	 * */
/*	public static byte[] compressStr(byte[] inBytes) throws IOException{
		if(inBytes == null || inBytes.length == 0){
			return inBytes;
		}
		
		long startTime = System.currentTimeMillis();
		
		ByteArrayOutputStream outOS = new ByteArrayOutputStream();
		GZIPOutputStream gzipOS = new GZIPOutputStream(outOS);
		gzipOS.write(inBytes);
		gzipOS.close();

//		byte[] res = Base64.encode(outOS.toByteArray(), Base64.DEFAULT);
		
		long endTime = System.currentTimeMillis();
		Log.d(TAG, "Compress time: " + (endTime-startTime) + " ms");
		
		return outOS.toByteArray();
	}*/
	
	public static byte[] compressStr(byte[] inBytes){
		if(inBytes == null || inBytes.length == 0){
			return inBytes;
		}
		long startTime = System.nanoTime();
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		Deflater compressor = new Deflater(Configuration.getInstance().deflateLevel);
		compressor.setInput(inBytes);
		compressor.finish();
		byte[] buf = new byte[2048];
		while(!compressor.finished()){
			int count = compressor.deflate(buf);
			bos.write(buf, 0, count);
		}
		compressor.end();
		long endTime = System.nanoTime();
		Log.d(TAG, "Compress time: " + (endTime-startTime) + " ns");
		
		return bos.toByteArray();
	}
	
	public static String unCompressStr(byte[] inBytes) throws DataFormatException{
		if(inBytes == null || inBytes.length == 0){
			return null;
		}
		long startTime = System.nanoTime();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		Inflater decompressor = new Inflater();
		decompressor.setInput(inBytes);
		byte[] buf = new byte[2048];
		while(!decompressor.finished()){
			int count = decompressor.inflate(buf);
			bos.write(buf, 0, count);
		}
		
		decompressor.end();
		long endTime = System.nanoTime();
		Log.d(TAG, "deCompress time: " + (endTime-startTime) + " ns");
		return bos.toString();
	}
	
	/*
	 * inStr is base64 encoded;
	 * */
	/*public static String unCompressStr(byte[] inBytes) throws IOException{		
		if(inBytes == null || inBytes.length == 0){
			return null;
		}
		
		long startTime = System.currentTimeMillis();

//		byte[] inBytes = Base64.decode(inStr.getBytes(), Base64.DEFAULT);
		
		ByteArrayOutputStream outOS = new ByteArrayOutputStream();
		ByteArrayInputStream inOS = new ByteArrayInputStream(inBytes);
		GZIPInputStream gzipOS = new GZIPInputStream(inOS);
		byte[] buffer = new byte[512];
		int n;
		while((n = gzipOS.read(buffer)) >= 0){
			outOS.write(buffer, 0, n);
		}

		long endTime = System.currentTimeMillis();
		Log.d(TAG, "deCompress time: " + (endTime-startTime) + " ms");
		
//		return outOS.toString();
		return outOS.toString();
	}*/
}
