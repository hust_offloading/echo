package hust.wisemigrate_libs.wrapper;

import java.io.Serializable;

public class RequestWrapper implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public final static byte REQUEST_TYPE_EXECUTE = 0x00;
	public final static byte REQUEST_TYPE_PING = 0x01; 
	public final static byte REQUEST_TYPE_APK_REGISTER = 0x02;
	
	public final static byte REQUEST_TYPE_FINI = 0x04;
	public final static byte REQUEST_TYPE_PARA_EMPTY = 0x05;
	public final static byte REQUEST_TYPE_PARA_INCACHE = 0x06;
	public final static byte REQUEST_TYPE_PARAS = 0x07;
	public final static byte REQUEST_TYPE_PARA_BITMAP = 0x08; 
	public final static byte REQUEST_TYPE_OBJECT_INCACHE = 0x09;
	public final static byte REQUEST_TYPE_OBJECT = 0x0A;
	
	public final static byte REQUEST_TYPE_MSG_STR = 0x0B;
	public final static byte REQUEST_TYPE_MSG_BYTES = 0x0C;
	
	public final static byte REQUEST_TYPE_MSG_PROXY_REAL = 0X0D;
}
