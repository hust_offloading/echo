package hust.wisemigrate_libs.wrapper;

import java.util.zip.CRC32;

public class ObjectCRC32 {
	
	public static long computeCRC32(String objJson){
		CRC32 crc = new CRC32();
		crc.update(objJson.getBytes());
		long res = crc.getValue();
		return res;
	}
	
	public static long computeCRC32(byte[] objBytes){
		CRC32 crc = new CRC32();
		crc.update(objBytes);
		return crc.getValue();
	}
	
	public static long computeCRC32(String[] strArr){
		StringBuilder builder = new StringBuilder();
		for(String str : strArr){
			builder.append(str);
		}
		
		CRC32 crc = new CRC32();
		crc.update(builder.toString().getBytes());
		return crc.getValue();
	}
}
