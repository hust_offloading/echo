package hust.wisemigrate_libs.wrapper;

import java.io.Serializable;

public class ResponseWrapper implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public final static byte RESPONSE_TYPE_PONG = 0x00;
	public final static byte RESPONSE_TYPE_APK_PRESENT = 0x01;
	public final static byte RESPONSE_TYPE_APK_REQUEST = 0x02;
	public final static byte RESPONSE_TYPE_EXECUTE_RESULT = 0x03;
	public final static byte RESPONSE_TYPE_EXECUTE_EXCEPTION = 0x04;
	public final static byte RESPONSE_TYPE_FOR_PROXY_REAL = 0x05;
	public final static byte RESPONSE_TYPE_EXECUTE_OBJSTATE = 0x06;
	
	public final static byte RESPONSE_TYPE_EXECUTE_RESULT_BITMAP = 0x07;
	
}
