package hust.wisemigrate_libs.wrapper;

import java.io.Serializable;

public class APKRegisterRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private final String name;
	private final int verCode;
	
	public APKRegisterRequest(String appName, int versionCode){
		this.name = appName;
		this.verCode = versionCode;
	}

	public String getAppName() {
		return name;
	}

	public int getVersionCode() {
		return verCode;
	}
}
