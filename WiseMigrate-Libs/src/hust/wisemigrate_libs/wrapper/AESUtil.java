package hust.wisemigrate_libs.wrapper;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;
import android.util.Log;

public class AESUtil {
	private final static String TAG = "AESUtil";
	private final static int JELLY_BEAN_4_2 = 17;
	
	private byte[] key = null;
	
	public AESUtil(String srcKey) throws NoSuchAlgorithmException, NoSuchProviderException{
		key = getRawKey(srcKey.getBytes());
	}
	
	public byte[] decrypt(byte[] data){
//		data = Base64.decode(data, Base64.DEFAULT);
//		Log.d(TAG, "base64 decoder: " + Arrays.toString(data));
		Log.d(TAG, "encrypt data: " + new String(data));
		SecretKeySpec sKeySpec = new SecretKeySpec(key, "AES");
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, sKeySpec);  
	        byte[] res = cipher.doFinal(data);
	        Log.d(TAG, "decrypt data: " + new String(res));
	        return res;
		} catch(Exception e){
			Log.d(TAG, "decrypt exception");
			e.printStackTrace();
		}
        
        return null; 
	}
	
	public byte[] encrypt(byte[] data){
		SecretKeySpec sKeySpec = new SecretKeySpec(key, "AES");
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, sKeySpec);
			byte[] res = cipher.doFinal(data);
//			Log.d(TAG, "encrypt data, before base64:" + Arrays.toString(res));
//			Log.d(TAG, "encrypt String, before base64:" + new String(res));
			/*res = Base64.encode(res, Base64.DEFAULT);
			Log.d(TAG, "encrypt data, after base64:" + Arrays.toString(res));
			Log.d(TAG, "encrypt String, after base64:" + new String(res));*/
			return res;
		}catch(Exception e){
			Log.d(TAG, "encrypt exception");
			e.printStackTrace();
		}
		
		return null;
	} 
	
	private byte[] getRawKey(byte[] seed) throws NoSuchAlgorithmException, NoSuchProviderException{
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		SecureRandom sr = null;  
		if (android.os.Build.VERSION.SDK_INT >= JELLY_BEAN_4_2) {  
            sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");  
        } else {  
            sr = SecureRandom.getInstance("SHA1PRNG");  
        }  
		sr.setSeed(seed);
		kgen.init(128, sr);
		SecretKey sKey = kgen.generateKey();
		byte[] key = sKey.getEncoded();
		return key;
	}
}
