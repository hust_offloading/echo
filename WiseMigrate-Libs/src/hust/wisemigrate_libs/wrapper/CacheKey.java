package hust.wisemigrate_libs.wrapper;

public class CacheKey {
	public long crcValue;
	public int len;
	
	public CacheKey(long crcValue, int len){
		this.crcValue = crcValue;
		this.len = len;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CacheKey other = (CacheKey) obj;
		if (crcValue != other.crcValue)
			return false;
		if (len != other.len)
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (crcValue ^ (crcValue >>> 32));
		result = prime * result + len;
		return result;
	}

	@Override
	public String toString() {
		return "CacheKey [crcValue=" + crcValue + ", len=" + len + "]";
	}
}
