package hust.wisemigrate_libs;

import hust.wisemigrate_libs.common.Configuration;
import hust.wisemigrate_libs.db.DBEntry;
import hust.wisemigrate_libs.db.DBHelper;
import hust.wisemigrate_libs.profilers.NetworkProfiler;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

/**
 * This is a dynamic decision engine that chooses the way of method executing,
 * the local or the remote. 
 * @author ll
 *
 */
public class OffloadingSolver {
	private static final String TAG = "OffloadingSolver";
	
	static final int EXECUTION_LOCATION_STATIC_LOCAL	= 1;
	static final int EXECUTION_LOCATION_DYNAMIC			= 2;
	static final int EXECUTION_LOCATION_STATIC_REMOTE	= 3;
	static final int USER_CARES_TIME_ENERGY				= 0;
	static final int USER_CARES_TIME_FIRST				= 1;
	static final int USER_CARES_ENERGY_FIRST			= 2;
	
	
	public static boolean ShouldOffload;
	
	private String currentNetworkType;
	private String currentNetworkSubType;
	private Context context;
	private String classMethodName;
	private DBHelper helper;
	
	private int regime;
	private int userChoice;
	private int scaling;
	
	OffloadingSolver(int regime) {
		Log.d(TAG, "OffloadingSolver Constructor.");
		this.regime = regime;
		//userChoice = EXECUTION_LOCATION_STATIC_REMOTE;
		userChoice = Configuration.getInstance().getDecisionStrategy();
		Log.d(TAG, "User choice is: " + String.valueOf(userChoice));
		scaling = 2;
	}
	
	/**
	 * Decide whether to execute remotely or locally
	 * 
	 * @return True if need to execute remotely, False if locally
	 */
	boolean executeRemotely(Context context, String classMethodName) {
		
		if(regime == EXECUTION_LOCATION_STATIC_LOCAL)
		{
			return false;
		}
		else if(regime == EXECUTION_LOCATION_STATIC_REMOTE)
		{
			//construct func make regime=remote fixedly
			return true;
		}
		else 
		{	//regime == EXECUTION_LOCATION_DYNAMIC
			
			this.context = context;
			this.classMethodName = classMethodName;
			
			ShouldOffload = getDecision();
			
			if(ShouldOffload) {
				Log.d(TAG, "Execute Remotely - True");
			} else {
				Log.d(TAG, "Execute Remotely - False");
			}
			
			return ShouldOffload;
			
		}
	}
	
	/**
	 * 
	 * @return	True if:
	 * 				1. meanExecTimeRemote < meanExecTimeLocal
	 * 				2. The method has never been executed remotely (meanExecTimeRemote == 0)
	 * 						(obviously in this point the connection is available)
	 * 						(the reason is that we want to explore the remote execution)
	 * 			False if:
	 * 				1. meanExecTimeRemote > meanExecTimeLocal
	 * 				2. The method has been executed remotely but never locally (just to have what to compare) 
	 */
	//TODO
	boolean getDecision() {
		
		currentNetworkType = NetworkProfiler.currentNetworkTypeName;
		currentNetworkSubType = NetworkProfiler.currentNetworkSubtypeName;
		
		long meanExecDurationLocally = 0;
		long meanExecDurationRemotely = 0;
		long meanEnergyConsumptionLocally = 0;
		long meanEnergyConsumptionRemotely = 0;
		
		helper = new DBHelper(context);
		helper.open();
		
		String selection = "methodName = ? AND execLocation = ? AND " + 
				"networkType = ? AND networkSubType = ?";
		String[] selectionArgsLocal = new String[] {classMethodName, "LOCAL", "", ""};
		String[] selectionArgsRemote = new String[] {classMethodName, "REMOTE", 
				currentNetworkType, currentNetworkSubType};
		
		Cursor cursorLocal = helper.getEntries(new String[]{"execTime", "energy"}, selection, selectionArgsLocal, null, null, null);
		
		if(cursorLocal.moveToFirst()) {
			meanExecDurationLocally = cursorLocal.getLong(cursorLocal.getColumnIndex(DBEntry.Record.COLUMN_EXECTIME));
			meanEnergyConsumptionLocally = cursorLocal.getLong(cursorLocal.getColumnIndex(DBEntry.Record.COLUMN_ENERGY));
		}
		
		Cursor cursorRemote = helper.getEntries(new String[]{"execTime", "energy"}, selection, selectionArgsRemote, null, null, null);
		
		if(cursorRemote.moveToFirst()) {
			meanExecDurationRemotely = cursorRemote.getLong(cursorRemote.getColumnIndex(DBEntry.Record.COLUMN_EXECTIME));
			meanEnergyConsumptionRemotely = cursorRemote.getLong(cursorLocal.getColumnIndex(DBEntry.Record.COLUMN_ENERGY));
		}
		
		//Close the database
		helper.close();
		
		if(userChoice == USER_CARES_TIME_ENERGY) {
			Log.d(TAG, "Strategy for both time and energy.");
			return ((meanExecDurationRemotely == 0) ? true : (meanExecDurationRemotely < meanExecDurationLocally)) &&
					((meanEnergyConsumptionRemotely == 0) ? true : (meanEnergyConsumptionRemotely < meanEnergyConsumptionLocally));
		}
		else if(userChoice == USER_CARES_TIME_FIRST) {
			Log.d(TAG, "Strategy for time reducing");
			return ((meanExecDurationRemotely == 0) ? true : (meanExecDurationRemotely < meanExecDurationLocally)) &&
					((meanEnergyConsumptionRemotely == 0) ? true : (meanEnergyConsumptionRemotely < scaling * meanEnergyConsumptionLocally));
		} else {
			Log.d(TAG, "Strategy for energy reducing");
			return ((meanEnergyConsumptionRemotely == 0) ? true : (meanEnergyConsumptionRemotely < meanEnergyConsumptionLocally)) &&
					((meanExecDurationRemotely == 0) ? true : (meanExecDurationRemotely < scaling * meanExecDurationLocally));
		}

	}
	
	public void setUserChoice(int userChoice) {
		this.userChoice = userChoice;
	}
}
