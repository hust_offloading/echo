package hust.wisemigrate_libs.common;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import android.content.res.Resources;
import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import hust.wisemigrate_libs.wrapper.MyObjectIOStream;



public class ApplicationContext {
	private final static String TAG = "ApplicationContext";
	
	private Resources mResources;
	private String mPackageName;
	private Context mContext;
	
	private static ApplicationContext appContext;
	
	public static SparseArray<MyObjectIOStream> CLMyIOStreamMap = new SparseArray<>();
	public static SparseArray<Long> timeWaitMap = new SparseArray<>();
	public static SparseArray<Long> timeTranMap = new SparseArray<>();
	public static SparseArray<Long> timeJsonMap = new SparseArray<>();
	
	private ApplicationContext() {
		Log.d(TAG, "Construct func()");
	}
	
	public synchronized static ApplicationContext getInstance() {
		if(appContext == null)
			appContext = new ApplicationContext();
		return appContext;
	}
	
	public void setResources(Resources resources) {
		mResources = resources;
	}
	
	public Resources getResources() {
		return mResources;
	}
	
	public void setPackageName(String packageName) {
		mPackageName = packageName;
	}
	
	public String getPackageName() {
		return mPackageName;
	}
	
	public void setContext(Context context) {
		mContext = context;
	}
	
	public Context getContext() {
		return mContext;
	}

}
