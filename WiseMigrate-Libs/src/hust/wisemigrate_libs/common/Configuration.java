package hust.wisemigrate_libs.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;

public class Configuration {
	private static final String TAG = "Configuration";
	
	private transient String xmlPath = null;
	private String ns = null;
	private String serverIp = null;
	private int serverPort = 0;
	
	private boolean networkProfilerEnable;
	private boolean deviceProfilerEnable;
	private boolean programProfilerEnable;
	private boolean GZIPEnable;
	private boolean cacheEnable;
	private int 	profilerCount;
	private boolean aesEnable;
	private boolean isProxyEnable;
	private String  aesKey                  = "123456";
	private int     cacheThreshold;
	private int     gzipThreshold;
	
	public int     deflateLevel = 1;
	public boolean isBitmapSpecific = true;
	/**
	 * 0: TIME_AND_ENERGY
	 * 1: TIME_FIRST
	 * 2: ENERGY_FIRST
	 */
	private int decisionStrategy;
	
	private static Configuration configuration;
	
	public Configuration(String configXmlPath){
		this.xmlPath = configXmlPath;
		initFromPath();
	}
	
	private void initFromPath(){
		try {
			Log.d(TAG, "init from path: " + xmlPath);
			InputStream is = new FileInputStream(xmlPath);
			XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
			parser.setInput(is, "utf-8");
			
			if(parser == null)
				Log.d(TAG, "Failed to get XmlpullParser!");
			parse(parser);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Configuration getInstance() {
		if(configuration == null){
			configuration = new Configuration();
			configuration.initFromAsserts();
		}
		
		return configuration;
	}
	
	private Configuration() {
	}
	

	private void initFromAsserts(){
		try {
			Log.d(TAG, "init from asserts");
			AssetManager aManager = ApplicationContext.getInstance().getContext().getAssets();
			//String[] lists = aManager.list(".");
			InputStream is = ApplicationContext.getInstance().getContext().getAssets().open("config.xml");
			XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
			parser.setInput(is, "utf-8");
			if(parser == null)
				Log.d(TAG, "Failed to get XmlpullParser!");
			parse(parser);
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			Log.v(TAG, e.toString());
			throw new RuntimeException("Failed to parse the config.xml!");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("IOException happened!");
		} catch (Exception e) {
			e.printStackTrace();
			Log.v(TAG, e.toString());
			throw new RuntimeException("Exception happened!");
		}
	}
	
	private void parse(XmlPullParser parser) throws XmlPullParserException, IOException{
		int eventType = parser.getEventType();
		//Log.d(TAG, "EventType: " + String.valueOf(eventType));
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if (eventType == XmlPullParser.START_TAG) {
				String tagName = parser.getName();
				//Log.d(TAG, "Tag name: " + tagName);
				if (tagName.equals("profilers")) {
					readProfilers(parser);
				} else if (tagName.equals("network")) {
					readNetwork(parser);
				} else if (tagName.equals("security")) {
					readSecurity(parser);
				} else if (tagName.equals("proxy")) {
					readProxy(parser);
				} else if (tagName.equals("decision")) {
					readDecision(parser);
				} else if(tagName.equals("gzip")) {
					readGZIP(parser);
				} else if(tagName.equals("cache")){
					readCache(parser);
				}
			}						
			eventType = parser.next();
		}
		
	}
		
	/**
	 * Read profilers enable options between <profilers></profilers>
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readProfilers(XmlPullParser parser) throws XmlPullParserException, IOException {
		//Log.d(TAG, "Read profilers");
		parser.require(XmlPullParser.START_TAG, null, "profilers");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			String enableOption = null;
			if (name.equals("network_profiler")) {
				enableOption = readText(parser);
				if (enableOption.equals("true"))
					networkProfilerEnable = true;
				else
					networkProfilerEnable = false;
			} else if (name.equals("device_profiler")) {
				enableOption = readText(parser);
				if (enableOption.equals("true"))
					deviceProfilerEnable = true;
				else
					deviceProfilerEnable = false;
			} else if (name.equals("program_profiler")) {
				enableOption = readText(parser);
				if (enableOption.equals("true"))
					programProfilerEnable = true;
				else
					programProfilerEnable = false;
			} else if (name.equals("profiler_count")) {
				String count = readText(parser);
				profilerCount = Integer.valueOf(count);
			}
			
		}
	}
	
	/**
	 * Read server ip and server port between tag <network></network>
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readNetwork(XmlPullParser parser) throws XmlPullParserException, IOException {
		//Log.d(TAG, "Read network");
		parser.require(XmlPullParser.START_TAG, ns, "network");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("server_ip")) {
				parser.require(XmlPullParser.START_TAG, ns, "server_ip");
				serverIp = readText(parser);
				parser.require(XmlPullParser.END_TAG, ns, "server_ip");
			} else if (name.equals("server_port")) {
				parser.require(XmlPullParser.START_TAG, ns, "server_port");
				String port = readText(parser);
				parser.require(XmlPullParser.END_TAG, ns, "server_port");
				serverPort = Integer.parseInt(port);
			}
		}
	}
	
	/**
	 * Read security options between tag <security></security>
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readSecurity(XmlPullParser parser) throws XmlPullParserException, IOException {
		//Log.d(TAG, "Read security");
		parser.require(XmlPullParser.START_TAG, ns, "security");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			String enableOption = null;
			if (name.equals("aes_encryption")) {
				parser.require(XmlPullParser.START_TAG, ns, "aes_encryption");
				enableOption = readText(parser);
				if (enableOption.equals("true"))
					aesEnable = true;
				else
					aesEnable = false;
			}else if(name.equals("aes_key")){
				aesKey = readText(parser);
				Log.d(TAG, "read aes_key: " + aesKey);
			}
		}
	}
	
	/**
	 * Read proxy options between tag <proxy></proxy>
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readProxy(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "proxy");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			String enableOption = null;
			if (name.equals("proxy_enable")) {
				parser.require(XmlPullParser.START_TAG, ns, "proxy_enable");
				enableOption = readText(parser);
				if (enableOption.equals("true"))
					isProxyEnable = true;
				else
					isProxyEnable = false;
			}
		}
	}
	
	/**
	 * Read gzip options between tag <gzip></gzip>
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readGZIP(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "gzip");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			String enableOption = null;
			if (name.equals("gzip_enable")) {
				parser.require(XmlPullParser.START_TAG, ns, "gzip_enable");
				enableOption = readText(parser);
				if (enableOption.equals("true"))
					GZIPEnable = true;
				else
					GZIPEnable = false;
			}else if(name.equals("gzip_threshold")){
				String str = readText(parser);
				gzipThreshold = Integer.valueOf(str);
			}
		}
	}
	
	/**
	 * Read caheEnable options between tag <cache></cache>
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readCache(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "cache");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			String enableOption = null;
			if (name.equals("cache_enable")) {
				parser.require(XmlPullParser.START_TAG, ns, "cache_enable");
				enableOption = readText(parser);
				if (enableOption.equals("true"))
					cacheEnable = true;
				else
					cacheEnable = false;
			}else if(name.equals("cache_threshold")){
				String str = readText(parser);
				cacheThreshold = Integer.valueOf(str);
			}
		}
	}
	
	
	/**
	 * Read decision engine options between tag <decision></decision>
	 * @param parser
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private void readDecision(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "decision");
		while (parser.next() != XmlPullParser.END_TAG) {
			if(parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			String strategy = null;
			if(name.equals("strategy")) {
				parser.require(XmlPullParser.START_TAG, ns, "strategy");
				strategy = readText(parser);
				if(strategy.equals("TIME_AND_ENERGY"))
					decisionStrategy = 0;
				else if(strategy.equals("TIME_FIRST"))
					decisionStrategy = 1;
				else if(strategy.equals("ENERGY_FIRST"))
					decisionStrategy = 2;		
			}
		}
	}
	
	// For the tags text, like <server_ip>192.168.1.1</server_ip>
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
    
    public String getServerIp() {
    	return serverIp;
    }
    
    public int getServerPort() {
    	return serverPort;
    }
    
    public int getProfilerCount() {
    	return profilerCount;
    }
    
    public boolean isDeviceProfilerEnable() {
    	return deviceProfilerEnable;
    }
    
    public boolean isProgramProfilerEnable() {
    	return programProfilerEnable;
    }
    
    public boolean isNetworkProfilerEnable() {
    	return networkProfilerEnable;
    }
    
    public boolean isEncrypted() {
    	return aesEnable;
    }
    
    public boolean isGZIPEnable() {
    	return GZIPEnable;
    }
    
    public boolean isCacheEnable(){
    	return cacheEnable;
    }
    
    
    public boolean isProxyEnable(){
    	return isProxyEnable;
    }
    
    
    
    public int getDecisionStrategy() {
    	return decisionStrategy;
    }
    
    public String getAESKey(){
    	return aesKey;
    }
    
    public int getCacheThreshold(){
    	return cacheThreshold;
    }
    
    public int getGzipThreshold(){
    	return gzipThreshold;
    }
    
    public String printConfigInfo() {
    	String configInfo = "Server IP: " + getServerIp() 
    			+ "; Server port: " + String.valueOf(getServerPort())
    			+ "; Network profiler enable: " + String.valueOf(isNetworkProfilerEnable())
    			+ "; Device profiler enable: " + String.valueOf(isDeviceProfilerEnable())
    			+ "; Program profiler enable: " + String.valueOf(isProgramProfilerEnable())
    			+ "; Profiler count: " + String.valueOf(getProfilerCount())
    			+ "; AES encryption enable: " + String.valueOf(isEncrypted())
    			+ "; Proxy enable: " + String.valueOf(isProxyEnable())
    			+ "; Decision strategy: " + String.valueOf(getDecisionStrategy());
    	
    	return configInfo;
    }

}
