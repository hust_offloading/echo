package hust.wisemigrate_libs.profilers;

import hust.wisemigrate_libs.ControlConstants;
import hust.wisemigrate_libs.common.Configuration;
import hust.wisemigrate_libs.db.DBEntry;
import hust.wisemigrate_libs.db.DBHelper;
import hust.wisemigrate_libs.profilers.phone.NoteConstants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class Profiler {
	private static final String TAG = "Profiler";
	
	//Used in the single mode 
	//private static int profilerRunning;
	//Used in the count mode
	private boolean mFirstStart;
	private long mPuretimeTotal;
	private boolean mCountMode;
	//In the count mode, mCount indicates the accumulative profilings;
	private int mCount;		
	
	public static final int REGIME_CLIENT = 1;
	public static final int REGIME_SERVER = 2;
	
	private ProgramProfiler progProfiler;
	private NetworkProfiler netProfiler;
	private DeviceProfiler devProfiler;
	private Context mContext;
	private int mRegime;
	private String mLocation;
	
	//private static final String logFileName = "/sdcard/WiseMigrateLog.txt";
	private static FileWriter logFileWriter;
	private DBHelper helper;
	
	
	private final int MIN_FREQ = 245760; // The minimum frequency for a specific machine
	private final int MAX_FREQ = 352000; // The maximum frequency for a specific machine
	
	private long totalEstimatedEnergy;
	private double estimatedCpuEnergy;
	private double estimatedScreenEnergy;
	private double estimatedWiFiEnergy;
	private double estimated3GEnergy;
	
	private int mSpeedSteps;
	
	public LogRecord lastLogRecord;
	
	public NoteConstants noteConstants;
	
	//A singleton instance
	private static Profiler profiler;
	
	public static Profiler getInstance(Context context) {
		if(null == profiler)
			profiler = new Profiler(context);
		return profiler;
	}
	
	private Profiler(Context context) {
		this.mContext = context;
		mRegime = REGIME_CLIENT;
		noteConstants = new NoteConstants();
		mSpeedSteps = noteConstants.getNumSpeedSteps();
		mFirstStart = false;
		mPuretimeTotal = 0;
		//profilerRunning = 0;
		mCount = Configuration.getInstance().getProfilerCount();
		Log.d(TAG, "mCount is: " + String.valueOf(mCount));
		if(mCount > 1)
			mCountMode = true;
		else
			mCountMode = false;
		
	}
	
	public void setRemine(int regime) {
		this.mRegime = regime;
	}
	
	public void setProfilers(ProgramProfiler progProfiler, NetworkProfiler netProfiler, DeviceProfiler devProfiler) {
		this.progProfiler = progProfiler;
		this.netProfiler = netProfiler;
		this.devProfiler = devProfiler;
	}
	
	public void startExecutionInfoTracking() {
		if(!mCountMode) {
			//profilerRunning ++;
			if(mRegime == REGIME_CLIENT) {
				//this.devProfiler.trackBatteryLevel();
				
				if(logFileWriter == null) {
					try {
						File logFile = new File(ControlConstants.LOGFILE);
						logFile.createNewFile();
						logFileWriter = new FileWriter(logFile, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			if(netProfiler != null) {
				netProfiler.startTransmittedDataCounting();
				mLocation = "REMOTE";
			} else {
				mLocation = "LOCAL";
			}
			
			Log.d(TAG, mLocation + " " + progProfiler.methodName);
			progProfiler.startExecutionInfoTracking();
			
			if(mRegime == REGIME_CLIENT) 
				devProfiler.startDeviceProfiling();	
		}			
		else {
			//mCount > 1
			if(!mFirstStart) {
				if(mRegime == REGIME_CLIENT) {
					this.devProfiler.trackBatteryLevel();
					
					if(logFileWriter == null) {
						try {
							File logFile = new File(ControlConstants.LOGFILE);
							logFile.createNewFile();
							logFileWriter = new FileWriter(logFile, true);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				
				if(netProfiler != null) {
					netProfiler.startTransmittedDataCounting();
					mLocation = "REMOTE";
				} else {
					mLocation = "LOCAL";
				}
				
				Log.d(TAG, mLocation + " " + progProfiler.methodName);
				progProfiler.startExecutionInfoTracking();
				
				if(mRegime == REGIME_CLIENT) 
					devProfiler.startDeviceProfiling();		
			}
			
			mFirstStart = true;
		}	
	}
	
	/**
	 * stop running profiling and log infomation
	 * @param pureExecTime
	 * @return
	 */
	public void stopAndLogExecutionInfoTracking(long pureExecTime) {
		if(!mCountMode) {
			//profilerRunning --;
			mPuretimeTotal = pureExecTime;
			if(mRegime == REGIME_CLIENT)
				devProfiler.stopAndCollectDeviceProfiling();
			
			progProfiler.stopAndCollectExecutionInfoTracking();
			
			if(netProfiler != null) 
				netProfiler.stopAndCollectTransmittedData();
			
			LogRecord record = new LogRecord(progProfiler, netProfiler, devProfiler);
			//record.pureDuration = pureExecTime;
			record.pureDuration = mPuretimeTotal;
			record.execLocation = mLocation;
			
			lastLogRecord = record;
			
			if(mRegime == REGIME_CLIENT) {
				
				estimateEnergyConsumption();
				
				record.energyConsumption = totalEstimatedEnergy;
				record.cpuEnergy = estimatedCpuEnergy;
				record.screenEnergy = estimatedScreenEnergy;
				record.wifiEnergy = estimatedWiFiEnergy;
				record.threeGEnergy = estimated3GEnergy;
				
				Log.d(TAG, "Log Record - " + record.toString());
				try {
					if(logFileWriter == null) {
						File logFile = new File(ControlConstants.LOGFILE);
						logFile.createNewFile();
						logFileWriter = new FileWriter(logFile);
					}
					
					logFileWriter.append(record.toString() + "\n");
					logFileWriter.flush();
				} catch(IOException e) {
					e.printStackTrace();
				}
				
				updateDB();
			}
		} 
		else {
			mPuretimeTotal += pureExecTime;
			mCount --;
			if (0 == mCount) {
				if(mRegime == REGIME_CLIENT)
					devProfiler.stopAndCollectDeviceProfiling();
				
				progProfiler.stopAndCollectExecutionInfoTracking();
				if(netProfiler != null) 
					netProfiler.stopAndCollectTransmittedData();
				
				LogRecord record = new LogRecord(progProfiler, netProfiler, devProfiler);
				//record.pureDuration = pureExecTime;
				record.pureDuration = mPuretimeTotal;
				record.execLocation = mLocation;
				
				lastLogRecord = record;		//For database updating, TODO
				
				if(mRegime == REGIME_CLIENT) {
					
					estimateEnergyConsumption();
					
					record.energyConsumption = totalEstimatedEnergy;
					record.cpuEnergy = estimatedCpuEnergy;
					record.screenEnergy = 0;
					record.wifiEnergy = estimatedWiFiEnergy;
					record.threeGEnergy = 0;
					
					Log.d(TAG, "Log Record - " + record.toString());
					try {
						if(logFileWriter == null) {
							File logFile = new File(ControlConstants.LOGFILE);
							logFile.createNewFile();
							logFileWriter = new FileWriter(logFile);
						}
						
						logFileWriter.append(record.toString() + "\n");
						logFileWriter.flush();
					} catch(IOException e) {
						e.printStackTrace();
					}
					
					updateDB();
				}
				
				//return record;
				//Reset mCount
				mCount = 5;
				mPuretimeTotal = 0;
			}
		}
		
	}
	
	public void updateDB() {
		helper = new DBHelper(mContext);
		long meanExecDuration;
		long meanEnergyConsumption;
		String[] selectionArgs;
		String selection = "methodName = ? AND execLocation = ? " +
				"AND networkType = ? AND networkSubType = ?";
		
		DBEntry entry = new DBEntry();
		entry.setMethodName(lastLogRecord.methodName);
		entry.setExecLocation(lastLogRecord.execLocation);
		
		if(lastLogRecord.execLocation.equals("REMOTE")) {
			entry.setNetworkType(lastLogRecord.networkType);
			entry.setNetworkSubType(lastLogRecord.networkSubtype);
			selectionArgs = new String[] {lastLogRecord.methodName, lastLogRecord.execLocation,
					lastLogRecord.networkType, lastLogRecord.networkSubtype};
		} else {
			entry.setNetworkType("");
			entry.setNetworkSubType("");
			selectionArgs = new String[] {lastLogRecord.methodName, lastLogRecord.execLocation,
					"", ""};
		}
		
		helper.open();
		Cursor cursor = helper.getEntries(new String[] {"execTime", "energy"}, selection, selectionArgs, 
				null, null, "execTime");
		
		if(cursor == null || !cursor.moveToFirst()) {
			entry.setExecTime(lastLogRecord.execDuration);
			entry.setEnergy(lastLogRecord.energyConsumption);
			helper.insertEntry(entry);
		} else {
			//Already move to the first row 
			long execTime = cursor.getLong(cursor.getColumnIndex(DBEntry.Record.COLUMN_EXECTIME));
			long energy = cursor.getLong(cursor.getColumnIndex(DBEntry.Record.COLUMN_ENERGY));
			meanExecDuration = (lastLogRecord.execDuration + execTime) / 2;
			meanEnergyConsumption = (lastLogRecord.energyConsumption + energy) / 2;
			entry.setExecTime(meanExecDuration);
			entry.setEnergy(meanEnergyConsumption);
			helper.updateEntry(entry, selection, selectionArgs);
		}
		
		helper.close();
	}
	
	public void estimateEnergyConsumption() {
		int duration = devProfiler.getSeconds(); //循环次数，每个小阶段
		Log.d(TAG, "Profiler in " + String.valueOf(duration) + " second");
		
		estimatedCpuEnergy = estimateCpuEnergy(duration);
		Log.d(TAG, "CPU energy: " + estimatedCpuEnergy + " mJ");
		
		estimatedScreenEnergy = estimateScreenEnergy(duration);
		Log.d(TAG, "Screen energy: " + estimatedScreenEnergy + " mJ");
		
		if(netProfiler != null){
			if(lastLogRecord.execLocation.equals("REMOTE") && lastLogRecord.networkType.equals("WIFI")){
				estimatedWiFiEnergy = estimateWiFiEnergy(duration);
				Log.d(TAG, "WiFi energy: " + estimatedWiFiEnergy + " mJ");
			}
			else{
					estimated3GEnergy = estimate3GEnergy(duration);
					Log.d(TAG, "3g energy: " + estimated3GEnergy + " mJ");
			}
		}
		
		totalEstimatedEnergy = (long) (estimatedCpuEnergy + estimatedScreenEnergy + estimatedWiFiEnergy + estimated3GEnergy);
		Log.i(TAG, "total energy consumed: " + totalEstimatedEnergy + " mJ");		
	}
	
	/**
	 * Estimate the Power for the CPU every send: P0, P1, P2, ..., Pt<br>
	 * where t is the execution time in seconds.<br>
	 * If we calculate the average power Pm = (P0 + P1 + ... + Pt) / t and multiply<br>
	 * by the execution time we obtain the Energy consumed by the CPU executing the method.<br>
	 * This is: E_cpu = Pm * t which is equal to: E_cpu = P0 + P1 + ... + Pt<br>
	 * NOTE: This is due to the fact that we measure every second.<br>
	 * 
	 * @param duration Duration of method execution
	 * @return The estimated energy consumed by the CPU (mJ)
	 * 
	 * @author Sokol
	 */
/*	private double estimateCpuEnergy(int duration)
	{
		double estimatedCpuEnergy = 0;
		double betaUh = 4.34;
		double betaUl = 3.42;
		double betaCpu = 121.46;
		byte freqL = 0, freqH = 0;
		int util;
		byte cpuON;
		
		for(int i = 0; i < duration; i++)
		{
			util = calculateCpuUtil(i);
			
			if(devProfiler.getFrequence(i) == MAX_FREQ)
				freqH = 1;
			else
				freqL = 1;

			*//**
			 * If the CPU has been in idle state for more than 90 jiffies<br>
			 * then decide to consider it in idle state for all the second
			 * (1 jiffie = 1/100 sec)
			 *//*
			cpuON = (byte) ((devProfiler.getIdleSystem(i) < 90) ? 1 : 0);
			
			estimatedCpuEnergy += (betaUh*freqH + betaUl*freqL)*util + betaCpu*cpuON;
			
//			Log.d("PowerDroid-Energy", "util freqH freqL cpuON power: " + 
//					util + "  " + freqH + "  " + freqL + "  " + cpuON + 
//					"  " + estimatedCpuEnergy + "mJ");
			
			Log.d(TAG, "CPU Power: " + estimatedCpuEnergy + "mJ");
			
			freqH = 0;
			freqL = 0;
		}
		
		return estimatedCpuEnergy;
	}*/
	
	/**
	 * Estimate the CPU energy consumption of the current process in duration seconds.
	 * @param duration, time in second
	 * @return
	 */
	private double estimateCpuEnergy(int duration) {
		double estimatedCpuEnergy = 0;
		int cpuON;
		int util;
		double[] cpuPowerRatio = noteConstants.cpuPowerRatios();
		double cpuIdlePower = noteConstants.cpuIdlePower();
		long totalCpuSpeedTime;
		long tmpCpuTime;
		long[] cpuSpeedStepTime;
		
		for(int i=0; i<duration; i++) {
			//四核，所以4*90 = 360
			cpuON = (devProfiler.getIdleSystem(i) < 360) ? 1:0;
			if(1 == cpuON) {
				double tmpEstimatedCpuEnergy = 0.0;
				tmpCpuTime = devProfiler.getPidCpuUsage(i)*10; //Convert to millsecond		
				cpuSpeedStepTime = devProfiler.getCpuSpeedStepTime(i);
				totalCpuSpeedTime = calculateTotalCpuSpeedTime(i);	
				
				for(int step=0; step<mSpeedSteps; step++) {
					double ratio = (double)cpuSpeedStepTime[step] / totalCpuSpeedTime;
					tmpEstimatedCpuEnergy += ratio * tmpCpuTime * cpuPowerRatio[step];
				}
				//util = calculateCpuUtil(i);
				//Log.d(TAG, "PID CPU util is: " + String.valueOf(util));
				//estimatedCpuEnergy = tmpEstimatedCpuEnergy * util / 1000;
				estimatedCpuEnergy += tmpEstimatedCpuEnergy / 1000;
			} else {
				estimatedCpuEnergy += cpuIdlePower;
			}
			
		}

		//Energy in mJ
		long voltage = devProfiler.getBatteryVoltage();
		if(voltage == 0)
			return estimatedCpuEnergy * noteConstants.getBatteryVoltage();
		else
			Log.d(TAG, "voltage mannual: 4.238");
			voltage = 4304305L;
			return estimatedCpuEnergy * voltage / 1000000;
		
	}
	
	
	private int calculateCpuUtil(int i)
	{
		return (int)Math.ceil(100 * devProfiler.getPidCpuUsage(i) / 
				devProfiler.getSystemCpuUsage(i));
	}
	
	private long calculateTotalCpuSpeedTime(int i) {
		long[] cpuSpeedStepTime = devProfiler.getCpuSpeedStepTime(i);
		long totalCpuSpeedTime=0;
		for(int n=0; n<mSpeedSteps; n++)
			totalCpuSpeedTime += cpuSpeedStepTime[n];
		if(0 == totalCpuSpeedTime)
			totalCpuSpeedTime = 1;
		return totalCpuSpeedTime;
	}
	
	//unused in current version
	public double estimateScreenEnergy(int duration)
	{
		double estimatedScreenEnergy = 0;
		double betaBrightness = 2.4;
		
		for(int i = 0; i < duration; i++)
			estimatedScreenEnergy += betaBrightness * devProfiler.getScreenBrightness(i);
		
		return estimatedScreenEnergy;
		
	}
	
	
	/**
	 * The WiFi interface can be (mainly) in two states: high_state or low_state<br>
	 * Transition from low_state to high_state happens when packet_rate > 15<br>
	 * packet_rate = (nRxPackets + nTxPackets) / s<br>
	 * RChannel: WiFi channel rate<br>
	 * Rdata: WiFi data rate<br>
	 * 
	 * @param duration Duration of method execution
	 * @return The estimated energy consumed by the WiFi interface (mJ)
	 * 
	 */
	private double estimateWiFiEnergy(int duration)
	{
		double estimatedWiFiEnergy = 0;
		boolean inHighPowerState = false;
		int nRxPackets, nTxPackets;
		double betaRChannel;
		//byte betaWiFiLow = 20; byte???
		double betaWiFiLow = noteConstants.wifiLowPower();
		double betaWiFiHigh;
		double Rdata;
		
		for(int i = 0; i < duration; i++)
		{
			nRxPackets = netProfiler.getWiFiRxPacketRate(i);
			nTxPackets = netProfiler.getWiFiTxPacketRate(i);
			Rdata = (netProfiler.getUplinkDataRate(i) * 8) / 1000000; // Convert from B/s -> b/s -> Mb/s
			
			// The Wifi interface transits to the high-power state if the packet rate
			// is higher than 15 (according to the paper of PowerTutor)
			// Then the transition to the low-power state is done when the packet rate
			// is lower than 8
			if(!inHighPowerState)
			{
				//if( nRxPackets + nTxPackets > 15 )
				if(nRxPackets + nTxPackets > noteConstants.wifiLowHighTransition())
				{
					inHighPowerState = true;
					betaRChannel = calculateBetaRChannel();
					//betaWiFiHigh = 710 + betaRChannel*Rdata;
					betaWiFiHigh = noteConstants.wifiHighPower() + betaRChannel*Rdata;
					estimatedWiFiEnergy += betaWiFiHigh;
				}
				else
					estimatedWiFiEnergy += betaWiFiLow;
			}
			else
			{
				//if(nRxPackets + nTxPackets < 8)
				if(nRxPackets + nTxPackets < noteConstants.wifiHighLowTransition())
				{
					inHighPowerState = false;
					estimatedWiFiEnergy += betaWiFiLow;
				}
				else
				{
					betaRChannel = calculateBetaRChannel();
					//betaWiFiHigh = 710 + betaRChannel*Rdata;
					betaWiFiHigh = noteConstants.wifiHighPower() + betaRChannel*Rdata;
					estimatedWiFiEnergy += betaWiFiHigh;
				}
			}
			
//			Log.d("PowerDroid-Energy", "nRxPackets nTxPackets: " + nRxPackets + "  " + nTxPackets);
			Log.d(TAG, "Wifi Power: " + estimatedWiFiEnergy);
		}
		
		return estimatedWiFiEnergy;
	}
	
	private double calculateBetaRChannel()
	{
		// The Channel Rate of WiFi connection (Mbps)
		int RChannel = netProfiler.getLinkSpeed();
		return 48 - 0.768*RChannel;
	}
	
	/**
	 * In the powerTutor paper the states of 3G interface are three: <b>idle, cell_fach</b> and <b>cell_dch</b><br>
	 * Transition from <b>idle</b> to <b>cell_fach</b> happens if there are data to send or receive<br>
	 * Transition from cell_fach to idle happens if no activity for 4 seconds<br>
	 * Transition from cell_fach to cell_dch happens when uplink_buffer > uplink_queue ore d_b > d_q<br>
	 * Transition from cell_dch to cell_fach happens if no activity for 6 seconds. 
	 * 
	 * @param duration Duration of method execution
	 * @return The estimated energy consumed by the 3G interface (mJ)
	 * 
	 * @author Sokol
	 */
	public double estimate3GEnergy(int duration)
	{
		double estimated3GEnergy = 0;
		int beta3GIdle = 3;
		int beta3GFACH = 300;
		int beta3GDCH = 470;
		if(netProfiler != null){
			for(int i = 0; i < duration; i++)
			{
				if(netProfiler.get3GActiveState(i) == NetworkProfiler.THREEG_IN_IDLE_STATE)
					estimated3GEnergy += beta3GIdle;
				else if(netProfiler.get3GActiveState(i) == NetworkProfiler.THREEG_IN_FACH_STATE)
					estimated3GEnergy += beta3GFACH;
				else 
					estimated3GEnergy += beta3GDCH;
			}
		}
				
		return estimated3GEnergy;
	}

}
