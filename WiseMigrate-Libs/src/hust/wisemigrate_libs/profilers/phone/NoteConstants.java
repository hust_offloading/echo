package hust.wisemigrate_libs.profilers.phone;

public class NoteConstants implements PhoneConstants{
	public static final String TAG = "NoteConstatns";
	
	protected double BATTERY_VOLTAGE = 3.7;
	
	public NoteConstants() {
	}
	
	public String modelName() {
		return "GT-N7000";
	}
	
	public double maxPower() {
		return 2500;
	}
	
	//Used as power unit for CPU
	//private static final double[] arrayCpuPowerRatios = {200.0, 170.0, 140.0, 113.7, 82.1, 55.4};
	//private static final double[] arrayCpuFreqs = {1400, 1200, 1000, 800, 500, 200};
	//private final double cpuIdlePower = 1.4;
	//private final int stepsNum = 4;
	
	//HW-G610
	private static final double[] arrayCpuPowerRatios = {223.0, 196.0, 163.0, 138.0};
	private static final double[] arrayCpuFreqs = {1209.0, 988.0, 754.0, 497.25};
	private static final double cpuIdlePower = 3;
	private static final double wifiLowPower = 4;
	private static final double wifiHighPower = 120;
	
	//Mi4 - xiaolong801
/*	private static final double[] arrayCpuPowerRatios = {66.6, 84, 90.8, 96, 105, 111.5, 117.3, 123.6, 134.5, 141.8, 148.5,
														 168.4, 168.4, 168.4, 168.4};
	
	private static final double[] arrayCpuFreqs = {};
	private static final double cpuIdlePower = 2.8;
	private static final double wifiLowPower = 4;
	private static final double wifiHighPower = 120;*/
	
	public double[] cpuPowerRatios() {
		return arrayCpuPowerRatios;
	}
	
	public double cpuIdlePower() {
		return cpuIdlePower;
	}
	
	
	public int getNumSpeedSteps() {
		return arrayCpuPowerRatios.length;
	}
	
	public double wifiLowPower() {
		//Use the value of wifi.on in profile_xml
		return 4; 
	}
	
	public double wifiHighPower() {
		//Use the value of wifi.active in profile_xml
		return 120;
	}
	
	public double wifiLowHighTransition() {
	    return 15;
	}

	public double wifiHighLowTransition() {
	    return 8;
	}
	
	//Unused
	public double[] cpuFreqs() {
		return arrayCpuFreqs;
	}
	
	private static final double[] arrayWifiLinkRatios = {
		47.122645, 46.354821, 43.667437, 43.283525, 40.980053, 39.44422, 38.676581,
	    34.069637, 29.462693, 20.248805, 11.034917, 6.427122
	};
	public double[] wifiLinkRatios() {
		return arrayWifiLinkRatios;
		
	}
	
	private static final double[] arrayWifiLinkSpeeds = {
		1, 2, 5.5, 6, 9, 11, 12, 18, 24, 36, 48, 54
	};
	
	public double[] wifiLinkSpeeds() {
		return arrayWifiLinkSpeeds;
	}
	
	public double getBatteryVoltage() {
		return BATTERY_VOLTAGE;
	}
	
}
