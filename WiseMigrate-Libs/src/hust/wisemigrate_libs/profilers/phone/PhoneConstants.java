package hust.wisemigrate_libs.profilers.phone;

public interface PhoneConstants {
	
	public double cpuIdlePower();
	
	/* Gives the coefficients at different cpu frequencies for the amount of
	 * power/cpu utilization the processor is using.
	 */
	public double[] cpuPowerRatios();
	
	/* Gives the frequency for each of the power ratios listed in
	 * cpuPowerRatios().
	 */
	public double[] cpuFreqs();
	
	/* Gives the power consumption of wifi in the low power state.
	 */
	public double wifiLowPower();

	/* Gives the base power consumption while the wifi is in high power mode.
	 */
	public double wifiHighPower();

	/* Gives the packet rate needed to transition from the low power state
	 * to the high power state.
	 */
	public double wifiLowHighTransition();

	/* Gives the packet rate needed to transition from the high power state
	 * to the low power state.
	 */
	public double wifiHighLowTransition();
	
	/**
	 * Get the number of cpu speed steps
	 */
	public int getNumSpeedSteps();

	
}
