package hust.wisemigrate_libs;

import android.os.Environment;

public class ControlConstants {
	
	//Directories Infos
	public static final String MNT_SDCARD 						= Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
	public static final String WISEMIGRATE_FOLDER				= MNT_SDCARD + "wisemigrate/";
	public static final String SERVER_CONFIG_FILE				= WISEMIGRATE_FOLDER + "config-server.dat";
	public static final String PHONE_CONFIG_FILE				= WISEMIGRATE_FOLDER + "config-phone.dat";
	public static final String FILE_NOT_OFFLOADED				= WISEMIGRATE_FOLDER + "notOffloaded";
	public static final String LOGFILE							= MNT_SDCARD + "wisemigrate-log.txt";
	
	public static final String SERVER_PORT					= "[SERVER PORT]";
	public static final String SERVER_IP						= "[SERVER IP]";
	public static final String IS_ENCRYPTED					= "[IS ENCRYPTED]";
	
	public static final int PING								= 11;
	public static final int PONG								= 12;
	public static final int EXECUTE 							= 13;
	
	// Communication Phone <-> Clone? Server?
	public static final int APK_REGISTER 						= 21;
	public static final int APK_PRESENT 						= 22;
	public static final int APK_REQUEST 						= 23;
	public static final int APK_SEND 							= 24;
}
