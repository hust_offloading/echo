package hust.wisemigrate_libs.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper {
	private static final String TAG = "DBHelper";
	private static final String DATABASE_NAME = "codiet.db";
	private static final int DATABASE_VERSION = 1;
	private static String SQL_CREATE_DB = "CREATE TABLE IF NOT EXISTS " + DBEntry.Record.TABLE_NAME + " ("
			+ DBEntry.Record._ID + " INTEGER PRIMARY KEY,"
			+ DBEntry.Record.COLUMN_METHOD + " TEXT,"
			+ DBEntry.Record.COLUMN_EXECLOCATION + " TEXT,"
			+ DBEntry.Record.COLUMN_NETWORKTYPE + " TEXT,"
			+ DBEntry.Record.COLUMN_NETWORKSUBTYPE + " TEXT,"
			+ DBEntry.Record.COLUMN_EXECTIME + " INTEGER,"
			+ DBEntry.Record.COLUMN_ENERGY + " INTEGER"
			+ ");";
	
	private MyDBHelper mDbHelper;
	private SQLiteDatabase db;
	
	public DBHelper(Context context) {
		if(null == context)
			Log.d(TAG, "Context is null!");
		mDbHelper = new MyDBHelper(context);
		//open();
	}
	
	public void open() {
		db = mDbHelper.getWritableDatabase();
	}
	
	public void close() {
		db.close();
	}
	
	public long insertEntry(DBEntry entry) {
		ContentValues values = new ContentValues();
		values.put(DBEntry.Record.COLUMN_METHOD, entry.getMethodName());
		values.put(DBEntry.Record.COLUMN_EXECLOCATION, entry.getExecLocation());
		values.put(DBEntry.Record.COLUMN_NETWORKTYPE, entry.getNetworkType());
		values.put(DBEntry.Record.COLUMN_NETWORKSUBTYPE, entry.getNetworkSubType());
		values.put(DBEntry.Record.COLUMN_EXECTIME, entry.getExecTime());
		values.put(DBEntry.Record.COLUMN_ENERGY, entry.getEnergy());
		
		long rowId = db.insert(DBEntry.Record.TABLE_NAME, null, values);
		if(rowId > 0)
			return rowId;
		
		throw new SQLException("Failed to insert new row!"); 
	}
	
	public int updateEntry(DBEntry entry, String whereClause, String[] whereArgs) {
		ContentValues values = new ContentValues();
		values.put(DBEntry.Record.COLUMN_METHOD, entry.getMethodName());
		values.put(DBEntry.Record.COLUMN_EXECLOCATION, entry.getExecLocation());
		values.put(DBEntry.Record.COLUMN_NETWORKTYPE, entry.getNetworkType());
		values.put(DBEntry.Record.COLUMN_NETWORKSUBTYPE, entry.getNetworkSubType());
		values.put(DBEntry.Record.COLUMN_EXECTIME, entry.getExecTime());
		values.put(DBEntry.Record.COLUMN_ENERGY, entry.getEnergy());
		
		return db.update(DBEntry.Record.TABLE_NAME, values, whereClause, whereArgs);
	}
	
	
	public Cursor getEntries(String[] columns, String selection, String[] selectionArgs,
			String groupBy, String having, String orderBy) {
		return db.query(DBEntry.Record.TABLE_NAME, columns, selection, selectionArgs, 
				groupBy, having, orderBy);
	}
	
	
	public boolean clearTable() {
		return db.delete(DBEntry.Record.TABLE_NAME, null, null) > 0;
	}
	
	static class MyDBHelper extends SQLiteOpenHelper {
		
		public MyDBHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		@Override 
		public void onCreate(SQLiteDatabase db) {
			Log.d(TAG, "MyDBHelper onCreate!");
			db.execSQL(SQL_CREATE_DB);
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + DBEntry.Record.TABLE_NAME);
			onCreate(db);
		}
	}
}
