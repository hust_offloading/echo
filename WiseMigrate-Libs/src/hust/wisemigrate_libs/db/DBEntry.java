package hust.wisemigrate_libs.db;

import android.provider.BaseColumns;

public class DBEntry {
	private String methodName;
	private String execLocation;
	private String networkType;
	private String networkSubType;
	private long execTime;
	private long energy;
	
	public String getMethodName() {
		return methodName;
	}
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public String getExecLocation() {
		return execLocation;
	}
	
	public void setExecLocation(String execLocation) {
		this.execLocation = execLocation;
	}
	
	public String getNetworkType() {
		return networkType;
	}
	
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	
	public String getNetworkSubType() {
		return networkSubType;
	}
 	
	public void setNetworkSubType(String networkSubType) {
		this.networkSubType = networkSubType;
	}
	
	public long getExecTime() {
		return execTime;
	} 
	
	public void setExecTime(long execTime) {
		this.execTime = execTime;
	}
	
	public long getEnergy() {
		return energy;
	}
	
	public void setEnergy(long energy) {
		this.energy = energy;
	}
	
	public static final class Record implements BaseColumns {
		
		//This class cannot be instantiated
		private Record() {}
		
		public static final String TABLE_NAME = "logs";
		
		/**
		 * Column name for the executed method name, including class name
		 */
		public static final String COLUMN_METHOD = "methodName";
		
		/**
		 * Column name for the location of a method executed
		 */
		public static final String COLUMN_EXECLOCATION = "execLocation";
		
		/**
		 * Column name for the network type
		 */
		public static final String COLUMN_NETWORKTYPE = "networkType";
		
		/**
		 * Column name for the network sub type
		 */
		public static final String COLUMN_NETWORKSUBTYPE = "networkSubType";
		
		/**
		 * Column name for the method execute time (in nano)
		 */
		public static final String COLUMN_EXECTIME = "execTime";
		
		/**
		 * Column name for the energy consumption
		 */
		public static final String COLUMN_ENERGY = "energy";
		
		
		
	}
}
