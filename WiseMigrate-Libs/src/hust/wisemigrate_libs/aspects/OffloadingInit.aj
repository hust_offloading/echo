package hust.wisemigrate_libs.aspects;


import hust.wisemigrate_libs.ServerHandler;
import hust.wisemigrate_libs.common.ApplicationContext;
import android.app.Activity;
import android.os.Looper;
import android.util.Log;

public aspect OffloadingInit {
	private static final String TAG = "OffloadingInit";
	
	pointcut activityOnCreate(Activity activity) :
		execution(void (Activity+).onCreate(..)) && this(activity) && if(!ServerHandler.isInit());
	
	before (final Activity activity) : activityOnCreate(activity) {
		Log.d(TAG, "Activity onCreate with aspect");
		
		ApplicationContext.getInstance().setResources(activity.getResources());
		ApplicationContext.getInstance().setPackageName(activity.getPackageName());
		ApplicationContext.getInstance().setContext(activity);
	
		Thread thread = new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				ServerHandler.initialize(activity);
				Looper.loop();
			}
		});
		thread.start();
	}
	
	//TODO, unregisterReceiver
	pointcut activityOnDestroy() :
		execution(void (Activity+).onDestroy(..)) && if(ServerHandler.isInit());
	
	before () :  activityOnDestroy() {
		Log.d(TAG, "Activity onDestroy with aspect");
		ServerHandler.getServerHandler().fini();
	}

}
