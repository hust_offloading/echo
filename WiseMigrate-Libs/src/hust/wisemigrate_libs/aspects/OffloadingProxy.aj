package hust.wisemigrate_libs.aspects;

import hust.wisemigrate_libs.wrapper.CacheKey;
import hust.wisemigrate_libs.wrapper.MyObjectIOStream;
import hust.wisemigrate_libs.wrapper.ObjectCRC32;
import hust.wisemigrate_libs.wrapper.ResponseWrapper;
import hust.wisemigrate_libs.ServerHandler;
import hust.wisemigrate_libs.common.Configuration;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import android.util.Log;
import android.util.SparseArray;

import com.google.gson.Gson;

public aspect OffloadingProxy perthis(proxyObjNew() || proxyMethodInvoke(IProxy)){
	private final static String TAG = "OffloadingProxy";
	private static AtomicInteger atomicCounter = new AtomicInteger(0);
	public  static Map<CacheKey, String> objRealJsonCacheMap = new Hashtable<>();
	private static Gson gson = new Gson();
	
	public static SparseArray<Long> timeWaitMap = null;
	public static SparseArray<Long> timeTranMap = null;
	public static SparseArray<Long> timeJsonMap = null;
	private static Object mIOStreamObject = null;
	
	private static int CLHashCode = 0;
	private long jsonTime = 0L;
	
	static {
		try {
//			CLHashCode = this.getClass().getClassLoader().hashCode();
			CLHashCode = OffloadingProxy.class.getClassLoader().hashCode();
//			Log.e(TAG, "proxy obj cl hashcode: " + CLHashCode);
	
			Class<?> appClazz = Class.forName("hust.wisemigrate_libs.common.ApplicationContext", true, Thread.currentThread().getContextClassLoader());
			
			Field field = appClazz.getDeclaredField("CLMyIOStreamMap");
			field.setAccessible(true);
			SparseArray<MyObjectIOStream> CLMIOStreamMap = (SparseArray<MyObjectIOStream>) field.get(null);
			
			
			mIOStreamObject = CLMIOStreamMap.get(CLHashCode);
			
			Field field2 = appClazz.getDeclaredField("timeWaitMap");
			field2.setAccessible(true);
			timeWaitMap = (SparseArray<Long>) field2.get(null);
			
			Field field3 = appClazz.getDeclaredField("timeTranMap");
			field3.setAccessible(true);
			timeTranMap =  (SparseArray<Long>) field3.get(null);
			
			Field field4 = appClazz.getDeclaredField("timeJsonMap");
			field4.setAccessible(true);
			timeJsonMap = (SparseArray<Long>) field4.get(null);
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	
	{
		jsonTime = 0L;
	}
	
	declare parents : (@UseProxy *) implements IProxy;
	
	private String IProxy._className = null;
	private CacheKey IProxy._cacheKey = null;
	private boolean IProxy._isEmpty = false;
	
	public CacheKey IProxy.getCacheKey(){
		return _cacheKey;
	}
	public void IProxy.setCacheKey(CacheKey key){
		this._cacheKey = key;
	}
	
	public String IProxy.getClassName(){
		return _className;
	}
	public void IProxy.setClassName(String name){
		this._className = name;
	}
	
	public boolean IProxy.isEmpty(){
		return _isEmpty;
	}
	public void IProxy.setIsEmpty(boolean bool){
		this._isEmpty = bool;
	}
	
	//this configuration is on client and complex situation, it will execute on server
	pointcut proxyObjNew() : call((@UseProxy *).new(..))
		&& if(ServerHandler.isInit() && ServerHandler.getController().isOnLine() && Configuration.getInstance().isProxyEnable());
	
	pointcut initProxy() : initialization((@UseProxy *).new(..))
		&& !within(OffloadingProxy) && if(ServerHandler.isInit() && ServerHandler.getController().isOnLine() && Configuration.getInstance().isProxyEnable());
	
	Object around() : proxyObjNew(){
		Log.d(TAG, "enter proxy new COUNT: " + atomicCounter.getAndAdd(1));
		IProxy objectReal = (IProxy)proceed();
		objectReal.setIsEmpty(false);
		
		IProxy objectProxy = (IProxy)proceed();
		
		Class<?> clazz = objectReal.getClass();
		objectProxy.setClassName(clazz.getClass().getName());
		makeEmpty(objectProxy);
		objectProxy.setIsEmpty(true);
//		Field[] fields = clazz.getDeclaredFields();
//		for(int i=0; i<fields.length; i++){
//			Log.d(TAG, "filed name: " + fields[i].getName());
//		}
		
		// compute key by real obj, the key won't change at any time;
		String objJson = toJson(objectReal);
		long crcValue = ObjectCRC32.computeCRC32(objJson);
		int len = objJson.length();
		CacheKey key = new CacheKey(crcValue, len);
		
		objectProxy.setCacheKey(key);
		Log.d(TAG, "proxy cacheKey: " + key.toString());
		objRealJsonCacheMap.put(key, objJson);
		
		return objectProxy;
	}
	
	private void makeEmpty(IProxy o){
		Log.d(TAG, "make empty: " + o.getClass().getName());
		Class<?> clazz = o.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for(int i=0; i<fields.length; i++){
//			Log.d(TAG, "filed name: " + fields[i].getName());
			String typeName = fields[i].getType().getName();
			fields[i].setAccessible(true);
			//seven primary
			if(!(typeName.equals("int")||typeName.equals("byte")||typeName.equals("boolean")
					||typeName.equals("float")||typeName.equals("char")||typeName.equals("long")
					||typeName.equals("short")||typeName.equals("double")) 
				&& !Modifier.isStatic(fields[i].getModifiers()) && !Modifier.isFinal(fields[i].getModifiers())){
				try {
//					Log.d(TAG, "set " + fields[i].getName() + " null");
					fields[i].set(o, null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	pointcut proxyMethodInvoke(IProxy p) : execution(* (@UseProxy *).*(..)) && this(p)
//		&& !(within(OffloadingProxy)) && if(Configuration.isProxyEnable() && p.isEmpty());  //at server client-config.xml not exist
		&& !(within(OffloadingProxy)) && if(p.isEmpty());
	
	Object around(IProxy p) : proxyMethodInvoke(p){
		Log.d(TAG, "proxy method invoke");
		if(p.isEmpty())
		{
			return proceed(getIProxyObj(p.getCacheKey(), p.getClass()));	
		}else {
			return proceed(p);
		}
	}
	
	pointcut proxyFieldVisit(IProxy proxy) : (get(* (@UseProxy *).*) || set(* (@UseProxy *).*)) && this(proxy)
		&& !(within(OffloadingProxy)) && if(proxy.isEmpty());
	
	Object around(IProxy proxy) : proxyFieldVisit(proxy){
		Log.d(TAG, "proxy field visit");
		if(proxy.isEmpty())
		{
			return proceed(getIProxyObj(proxy.getCacheKey(), proxy.getClass()));
		}else {
			return proceed(proxy);
		}
		
	}
	
	private IProxy getIProxyObj(CacheKey key, Class<?> clazz)
	{
		/*Log.d(TAG, "method invoke proxy cacheKey:" + key);
		Log.d(TAG, "p.getClass: " + clazz);
		
		Log.d(TAG, "thread classLoader hashcode: " + Thread.currentThread().getContextClassLoader().hashCode());
		Log.d(TAG, "thread classLoader: " + Thread.currentThread().getContextClassLoader());
		
		Log.d(TAG, "configuration loader hashcode: " + Configuration.class.getClassLoader().hashCode());
		Log.d(TAG, "configuration loader: " + Configuration.class.getClassLoader());
		
		Log.d(TAG, "configuration loader father loader hashcode: " + Configuration.class.getClassLoader().getParent().hashCode());
		Log.d(TAG, "configuration loader father loader: " + Configuration.class.getClassLoader().getParent());
		
		Log.d(TAG, " ApplicationContext classLoader hashcode: " + ApplicationContext.getInstance().getClass().getClassLoader().hashCode());
		Log.d(TAG, " ApplicationContext classLoader: " + ApplicationContext.getInstance().getClass().getClassLoader());
		
		Log.d(TAG, "this class hashcode: " + this.getClass().getClassLoader().hashCode());
		Log.d(TAG, "this class : " + this.getClass().getClassLoader());*/
		
		if(objRealJsonCacheMap.containsKey(key))
		{
			//client, this situation may happen when connection is lost suddenly? yes or no?
			//server, this is executed before, so cache has the real object
			Object objReal = fromJson(objRealJsonCacheMap.get(key), clazz);
			return (IProxy)objReal;
		}else {
			//get from the client
			//TO DO
			//get server ApplicationContext
			Log.d(TAG, "get obj from client");
			return getRealObj(key, clazz);
		}
	}
	
	private IProxy getRealObj(CacheKey key, Class<?> clazz){
		jsonTime = 0L;
		try{
			// send request for real object
			writeByte(ResponseWrapper.RESPONSE_TYPE_FOR_PROXY_REAL);
			writeString(toJson(key));
			long startWait = System.nanoTime();
			
			//recv real obj from client
			byte msgType = readByte();
			long endWait = System.nanoTime();
			long netWaitTime = (endWait - startWait);
			
			String inObjJson = readString();
			long endRecv = System.nanoTime();
			long netTransTime = (endRecv - endWait);
			
			updateTime(netWaitTime, netTransTime);
			Log.d(TAG, "real obj String: " + inObjJson);
			Object objReal = fromJson(inObjJson, clazz);
			
			return (IProxy)objReal;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private void updateTime(long waitTime, long transTime){
		long newWaitTime = timeWaitMap.get(CLHashCode, 0L) + waitTime;
		timeWaitMap.put(CLHashCode, newWaitTime);
		
		long newTransTime = timeTranMap.get(CLHashCode, 0L) + transTime;
		timeTranMap.put(CLHashCode, newTransTime);
		
		long newJsonTime = timeJsonMap.get(CLHashCode, 0L) + jsonTime;
//		Log.e(TAG, "update jsonTime: " + jsonTime);
		timeJsonMap.put(CLHashCode, newJsonTime);
	}
	
	private static void writeByte(byte val) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
//		Class<?>[] paraTypes = {int.class};
		Method method = mIOStreamObject.getClass().getDeclaredMethod("writeByte", byte.class);
		method.setAccessible(true);
		method.invoke(mIOStreamObject, val);
	}
	
	private static void writeString(String keyJson) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method method = mIOStreamObject.getClass().getDeclaredMethod("writeString", String.class);
		method.setAccessible(true);
		method.invoke(mIOStreamObject, keyJson);
	}
	
	private static String readString() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method method = mIOStreamObject.getClass().getDeclaredMethod("readString", null);
		method.setAccessible(true);
		return (String)method.invoke(mIOStreamObject, null);
	}
	
	private static byte readByte() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method method = mIOStreamObject.getClass().getDeclaredMethod("readByte", null);
		method.setAccessible(true);
		return (byte)method.invoke(mIOStreamObject, null);
	}
	
	private String toJson(Object obj){
		long startJson = System.nanoTime();
		String res = gson.toJson(obj);
		jsonTime += (System.nanoTime() - startJson);
		
		return res;
	}
	
	private Object fromJson(String objJson, Class<?> clazz){
		long startJson = System.nanoTime();
		Object obj = gson.fromJson(objJson, clazz);
		jsonTime += (System.nanoTime() - startJson);
		return obj;
	}
	
	public Map<CacheKey, String> getObjRealJsonCacheMap(){
		return objRealJsonCacheMap;
	}
}
