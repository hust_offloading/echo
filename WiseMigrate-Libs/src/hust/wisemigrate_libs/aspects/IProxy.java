package hust.wisemigrate_libs.aspects;

import hust.wisemigrate_libs.wrapper.CacheKey;

public interface IProxy {
	public CacheKey getCacheKey();
	public void setCacheKey(CacheKey key);
	
	public String getClassName();
	public void setClassName(String className);
	
	public boolean isEmpty();
	public void setIsEmpty(boolean bool);
}
