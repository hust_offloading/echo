package hust.wisemigrate_libs.aspects;

import hust.wisemigrate_libs.OffloadingController3;
import hust.wisemigrate_libs.ServerHandler;
import hust.wisemigrate_libs.common.ApplicationContext;
import hust.wisemigrate_libs.profilers.DeviceProfiler;
import hust.wisemigrate_libs.profilers.NetworkProfiler;
import hust.wisemigrate_libs.profilers.Profiler;
import hust.wisemigrate_libs.profilers.ProgramProfiler;

import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.aspectj.lang.reflect.MethodSignature;

import android.util.Log;


public aspect OffloadingService perthis(doRemoteable(Object) || profileExecution()){
	private static final String TAG = "OffloadingService.aj";
	private static AtomicInteger enterInCount = new AtomicInteger(0);
	private static ExecutorService executorService = Executors.newSingleThreadExecutor();
	private CountDownLatch countDownLatch;
	private OffloadingController3 controller;
	private Object result;
	//private boolean remoting = false;
	
	//Members for profiling
	private long mStartExecution;
	private long mEndExecution;
	private ProgramProfiler mProgProfiler;
	private DeviceProfiler mDevProfiler;
	private Profiler mProfiler;

	//if method return void ?
	pointcut doRemoteable(Object object) :
		execution (* *(..)) && @annotation(Offloadable) 
					&& this(object) 
					&& !within(OffloadingService) 
					&& if(ServerHandler.isInit() && ServerHandler.getController().isOnLine());
	
	Object around(final Object object) : doRemoteable(object) {
		//count for multi-thread, but waste some time to get the lock
		Log.d(TAG, "Enter into the advice of doRemoteable! - " + enterInCount.addAndGet(1));
		
		try {
			Method method = ((MethodSignature)thisJoinPoint.getSignature()).getMethod();
//			Log.d(TAG, "method to be executed: " + method.getName());
			Class<?>[] pTypes = method.getParameterTypes();
			Object[] pValues = thisJoinPoint.getArgs();
			
			long startRx = NetworkProfiler.getProcessRxBytes();
			long startTx = NetworkProfiler.getProcessTxBytes();
			
			Object res = invoking(method, pTypes, pValues, object);
			
			long rxBytes = NetworkProfiler.getProcessRxBytes() - startRx;
			long txBytes = NetworkProfiler.getProcessTxBytes() - startTx;
			Log.d(TAG, "Transmit bytes: " + (rxBytes + txBytes) + "bytes");
			
			return res;
		} catch (Throwable t) {
			Log.e(TAG, t.toString());
			Log.v(TAG, "Using proceed(o) to execute the method!");
			return proceed(object);
		}		
	}
	
	pointcut profileExecution() :
		execution (* *(..)) && @annotation(Offloadable)
					&& !within(OffloadingService)
					&& if(ServerHandler.isInit() && !ServerHandler.getController().isOnLine()); 
	
	before() : profileExecution() {
		Method method = ((MethodSignature)thisJoinPoint.getSignature()).getMethod();
		Class<?> cls = thisJoinPoint.getClass();
		Log.d(TAG, "Before the method to be profiled: " + method.getName());
		Log.d(TAG, "Signature: " + thisJoinPoint.getSignature().getName());
		Log.d(TAG, "Class:" + cls.getName());
		mProgProfiler = ProgramProfiler.getInstance();
		mProgProfiler.setMethodName(method.getName());
		
		mDevProfiler = DeviceProfiler.getInstance(ApplicationContext.getInstance().getContext());
		mProfiler = Profiler.getInstance(ApplicationContext.getInstance().getContext());
		
		mProfiler.setProfilers(mProgProfiler, null, mDevProfiler);
		mProfiler.setRemine(OffloadingController3.REGIME_CLIENT);
		mProfiler.startExecutionInfoTracking();
		mStartExecution = System.nanoTime();
	} 
	
	after() : profileExecution() {
		Method method = ((MethodSignature)thisJoinPoint.getSignature()).getMethod();
		Log.d(TAG, "After the method profiled: " + method.getName());
		long pureExecutionDuration = System.nanoTime() - mStartExecution;
		Log.d(TAG, "LOCAL " + method.getName()
				+ ": Actual Invocation duration - " + pureExecutionDuration / 1000000
				+ "ms");
		mProfiler.stopAndLogExecutionInfoTracking(pureExecutionDuration);
	}
						
	
	public Object invoking(final Method method, final Class<?> []pTypes, 
			final Object[] pValues, final Object object) throws Throwable{

		final ThrowableWrapper throwableWrapper = new ThrowableWrapper();
		
		/* controller is singleton, as well as ServerHandler
		 * */
		controller = ServerHandler.getController();  
		countDownLatch = new CountDownLatch(1);
		Runnable runnable = new Runnable(){
			public void run() {
				try {
					synchronized(controller) {
						result = controller.execute(method, pTypes, pValues, object);
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.e(TAG, "Invoking thread exception: " + e.toString());
					throwableWrapper.setThrowable(e);
				}					
				countDownLatch.countDown();
			}
		};
		executorService.execute(runnable);
		countDownLatch.await(); //wait thread finish
		
		if(null != throwableWrapper.getThrowable()) {
			throw throwableWrapper.getThrowable();
		}
		return result;		
	}
	
	private static class ThrowableWrapper {
		private Throwable throwable;
		public Throwable getThrowable() {
			return throwable;
		}
		public void setThrowable(Throwable throwable) {
			this.throwable = throwable;
		}
	}
}
