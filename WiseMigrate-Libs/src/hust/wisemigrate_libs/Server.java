package hust.wisemigrate_libs;
/**
 * Wrapper of server state, acting like a clone. Maybe too simple
 * @author ll
 *
 */
public class Server {
	private String	ip;			//Ip address for client connecting
	private int		port;		//server port waiting for connecting
	
	public Server(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
}
