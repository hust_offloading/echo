# README #

This work is supported by National Natural Science Foundation of China under grant No. 61502103. You can find more information in the paper "Echo: An Edge-Centric Code Offloading System with Quality of Service Guarantee".
（本工作得到中国国家自然科学基金的支持61502103）

## Permission needed in AndroidManifest.xml
* <uses-permission android:name="android.permission.INTERNET" />
* <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
* <uses-permission android:name="android.permission.READ_PHONE_STATE" />
* <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
* <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
