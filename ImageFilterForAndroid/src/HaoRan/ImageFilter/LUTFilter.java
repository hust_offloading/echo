/* 
 * HaoRan ImageFilter Classes v0.3
 * Copyright (C) 2012 Zhenjun Dai
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation.
 */

package HaoRan.ImageFilter;

import HaoRan.ImageFilter.IImageFilter.Function;
import android.util.Log;


public class LUTFilter implements IImageFilter{

	
	protected int[]  m_LUT = new int[256] ;
	
	protected int InitLUTtable (int nLUTIndex){ 
		return nLUTIndex;
	}
	 
	public Image process(Image imageIn)
	{
		for (int i=0 ; i <= 0xFF ; i++)
			m_LUT[i] = InitLUTtable (i) ;
		int r, g, b;
		for(int x = 0 ; x < (imageIn.getWidth() - 1) ; x++){
			for(int y = 0 ; y < (imageIn.getHeight() - 1) ; y++){
				r = imageIn.getRComponent(x, y);
				g = imageIn.getGComponent(x, y);
				b = imageIn.getBComponent(x, y);
				
				imageIn.setPixelColor(x, y, Image.SAFECOLOR(m_LUT[r]), Image.SAFECOLOR(m_LUT[g]), Image.SAFECOLOR(m_LUT[b]));
			}
		}
		return imageIn;
	}
	
	public Image advProcess(Image imageIn){
    	long initBitmapStart = System.currentTimeMillis();
    	imageIn.initBitmap();
    	long initBitmapEnd = System.currentTimeMillis();
    	Log.d("VideoFilter 1", "init Bitmap time: " + (initBitmapEnd - initBitmapStart) + "ms");
    	
    	imageIn = process(imageIn);
    	
    	long cpStart = System.currentTimeMillis();
        imageIn.copyPixelsFromBuffer();
        long cpEnd = System.currentTimeMillis();
        Log.d("VideoFilter 1", "cp from buf time: " + (cpEnd - cpStart) + " ms");   
        
        return imageIn;
    }
}